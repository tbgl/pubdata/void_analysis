#ifndef VOLMAP_H_
#define VOLMAP_H_

void* create_molecules (char* filename);
void* create_volmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz, int max_sigma);
void* create_wca_volmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz);
void* create_bitvolmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz);
void clean_molecules (void* _mol);
void* create_volmap_sel (void* _handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, int Nx, int Ny, int Nz, double skin, int max_sigma);
void* create_wca_volmap_sel (void* _handler, int* mol_ids, int num_mols, void* _mol,
float* coords, int Nx, int Ny, int Nz, double skin);
void* create_bitvolmap_sel (void* _handler, int* mol_ids, int num_mols, void* _mol,
float* coords, int Nx, int Ny, int Nz, double skin);
void* create_phi_map (void* _mol, float* coords, double* cell, float rho, float rc, int Nx, int Ny, int Nz);
void* create_phi_map_sel (void* handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, float rho, float rc, int Nx, int Ny, int Nz, float skin);
void* create_phi_map_gaussian (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz, int _max_sigma);
void* create_phi_map_gaussian_sel (void* _handler, int* mol_ids, int num_mols, void* _mol,
float* coords, int Nx, int Ny, int Nz, double skin, int _max_sigma);
void* create_radius_map(void* _mol, float* coords, double* cell,
float cut_off, float rc, float probe, int Nx, int Ny, int Nz);
//void* create_volmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz, 
/*void* _cluster_handler, int* mol_id, int num_mols)
{
  
}*/
#endif
