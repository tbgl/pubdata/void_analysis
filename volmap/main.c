#include <stdlib.h>
#include <stdio.h>
#include "traj_dcd_handler.h"
#include "volmap.h"
#include "grid_reader.h"
int main (int argc, char** argv)
{
  if (argc < 6)
    {
      fprintf (stderr, "error arguments\n");
      exit (-1);
    }
  void* traj_handler = create_traj_dcd_handler (argv[1]); 
  read_dcd (traj_handler, 1400, 1401, 1);

  void* molecules = create_molecules (argv[2]); 
  float* coords = get_coords (traj_handler, 0);
  double* cell  = get_unitcell (traj_handler, 0);
  void* grid = create_volmap (molecules, coords, cell, 
                              atoi(argv[3]), atoi(argv[4]), atoi(argv[5]),4);

  grid_destroy (grid);
  clean_molecules (molecules);
  clean_traj_dcd_handler (traj_handler);

  return 0;
}
