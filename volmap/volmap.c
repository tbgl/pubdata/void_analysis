#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "util.h"
#include "grid_reader.h"
#include "cluster_analysis.h"
//#define _MAX_SIGMA 4
//#define _MAX_SIGMA 1
typedef struct Interpolate
{
  int num_types;
  int** range;
  int*  num_weights;
  double** weights;
  int** offsets;
  int max_sigma;
} Interpolate;

typedef struct Molecules
{
  int num;
  int num_types;
  int* type;
  float* mass;  
  float* radius;
  //double cell[6];
} Molecules;

#define MY_PI 3.14159265358979323846
#define SQRT_MY_PI 1.77245385091
#define SQRT2 1.4142135623731
#define POW_TWO_1OVER6 1.122462048309373

#ifndef NORMALIZED
static void _normalize_to_one (double* data, int Nx, int Ny, int Nz)
{
  long long int ncells = Nx*Ny*Nz;
  double max = 0.;
  for (long long int i = 0; i < ncells; ++i)
    {
      if (data[i] > max)
          max = data[i];
    }
  if (max != 0.)
    {
      for (long long int i = 0; i < ncells; ++i)
        {
          data[i] /= max;
        }
    }
}
#endif

static void _determine_ranges (Interpolate* inter, void* grid, Molecules* mol, int _max_sigma)
{
  int num_types = mol->num_types;
  inter->num_types = num_types;

  inter->range   = (int**) malloc (sizeof (int*) * num_types);
  memset (inter->range, 0, sizeof (int*) * num_types);

  inter->num_weights = (int*) malloc (sizeof (int) * num_types);
  memset(inter->num_weights, 0, sizeof (int) * num_types);

  inter->offsets = (int**) malloc (sizeof (int*) * 3 * num_types);
  memset (inter->offsets, 0, sizeof(int*) * 3 * num_types);

  inter->weights = (double**) malloc (sizeof (double*) * num_types);
  memset (inter->weights, 0, sizeof (double*) * num_types);

  inter->max_sigma = _max_sigma;
  double** basis = get_grid_basis (grid);
  float delta[3];
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  for (int i = 0; i < num_types; ++i)
    {
      float rad = mol->radius[i];
      float cut_off = rad;
      cut_off = inter->max_sigma * cut_off;
      inter->range[i] = realloc (inter->range[i], 3*sizeof (int));
      int* range = inter->range[i];
      range[0] = (int) ceil (cut_off/delta[0]);
      range[1] = (int) ceil (cut_off/delta[1]);
      range[2] = (int) ceil (cut_off/delta[2]);

      for (int x = -range[0]; x < range[0]; ++x)
        {
 	  float dx = x*delta[0];
          for (int y = -range[1]; y < range[1]; ++y)
            {
	      float dy = y*delta[1];
              for (int z = -range[2]; z < range[2]; ++z)
	        {
		  float dz = z*delta[2];
		  float dr2 = dx*dx + dy*dy + dz*dz;

                  if(dr2 < cut_off*cut_off)
                    {
                      int num = ++inter->num_weights[i];
                      inter->offsets[i] = (int*) realloc (inter->offsets[i], sizeof (int) * 3 * num);
                      inter->offsets[i][3*(num-1)+0] = x;
                      inter->offsets[i][3*(num-1)+1] = y;
                      inter->offsets[i][3*(num-1)+2] = z;
                    }
	        }
            }
        }
    }
}

static void _read_beads (Molecules* mol, FILE* fp)
{
  char** argv = NULL;
  int argc = 0;
  char* line = NULL;
  size_t nbytes = 0;
  if (getline (&line, &nbytes, fp) < 0)
    {
      fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
      exit (-1);
    }
  trim_both_sides (line);
  extract_arguments (&argv, &argc, line);
  if (argc != 1)
    {
      fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
      exit (-1);
    }
  int* is_set = NULL;
  mol->num_types = atoi(argv[0]);
  is_set = (int*) malloc (sizeof(int) * mol->num_types);
  memset (is_set , 0, sizeof(int) * mol->num_types);
  //mol->type   = (int*)   malloc (sizeof (int)   * mol->num_types);
  mol->mass   = (float*) malloc (sizeof (float) * mol->num_types);
  mol->radius = (float*) malloc (sizeof (float) * mol->num_types);
  int num_read = 0;
  while (num_read < mol->num_types)
    {
      getline (&line, &nbytes, fp);
      trim_both_sides(line);
      if (strlen (line) == 0)
          continue;
      extract_arguments (&argv, &argc, line);
      if (argc != 3)
        {
          fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
          exit (-1); 
        }
        int t = atoi (argv[0]);
        if (is_set[t] == 1)
          {
            fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__); 
            exit (-1);
          }
        mol->mass[t]   = atof (argv[1]);
        mol->radius[t] = atof (argv[2]);
        is_set[t] = 1;
        num_read++;
    }
  for (int t = 0; t < mol->num_types; ++t)
    {
      if (is_set[t] != 1)
        {
          fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
          exit (-1);       
        }    
      //fprintf(stderr, "%f %f\n", mol->mass[t], mol->radius[t]);   
    }
  free(is_set);
  for (int i = 0; i < argc; ++i)
    free (argv[i]);
  free (argv);
  free (line);
}

static void _read_molecules (Molecules* mol, FILE* fp)
{
  char* line = NULL;
  size_t nbytes = 0;
  char** argv = NULL;
  int argc = 0; 
  if (getline (&line, &nbytes, fp) < 0)
    {
      fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
      exit (-1);
    }
  trim_both_sides (line);
  extract_arguments (&argv, &argc, line);
  if (argc != 1)
    {
      fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
      exit (-1);
    }
  mol->num = atoi (argv[0]);
  int* is_set = NULL;
  mol->type = (int*) malloc (sizeof (int) * mol->num);
  is_set = (int*) malloc (sizeof (int) * mol->num);
  memset (is_set, 0, sizeof(int) * mol->num);

  int num_read = 0;
  while (num_read < mol->num)
    {
      getline (&line, &nbytes, fp);
      trim_both_sides(line);
      if (strlen (line) == 0)
          continue;
      extract_arguments (&argv, &argc, line);
      if (argc % 2 != 0)
        {
          fprintf (stderr, "%s\n", line);
          fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
          exit (-1); 
        }
      for (int i = 0; i < argc; i+=2)
        {
          int t = atoi (argv[i]);
          if (is_set[t])
            {
              fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
              exit (-1);
            }
          mol->type[t] = atoi (argv[i+1]);
          is_set[t] = 1;
          num_read++;
        }
    }
  for (int t = 0; t < mol->num; ++t)
    {
      if (!is_set[t])
        {
          fprintf (stderr, "error format %s %d\n", __FILE__, __LINE__);
          exit (-1);       
        }      
    }
  for (int i = 0; i < argc; ++i)
    free (argv[i]);
  free (argv);
  free (line);
  free (is_set);
}

void* create_molecules (char* filename)
{
  Molecules* mol = (Molecules*) malloc (sizeof (Molecules));
  memset (mol, 0, sizeof(Molecules));
  FILE* fp = fopen (filename, "rt");
  if (fp == NULL)
    {
      fprintf (stderr, "no file \n");
      exit (-1);
    }
  char* line = NULL;
  size_t nbytes = 0;
  char** argv = NULL;
  int argc = 0;
  while (getline (&line, &nbytes, fp) > 0)
    {
      trim_both_sides(line);
      if (strlen (line) == 0)
          continue;
      extract_arguments (&argv, &argc, line);
      if (!strcmp (argv[0], "Beads"))
        {
          _read_beads (mol, fp);
        }
      else if (!strcmp (argv[0], "Molecules"))
        {
          _read_molecules (mol, fp);
        }
      else
        {
          fprintf (stderr, "%s\n", line);
          fprintf (stderr, "Not recognize the title\n");
          exit (-1);
        }
    }
  for (int i = 0; i < argc; ++i)
    free (argv[i]);
  free (argv);
  free (line);
  fclose(fp);
  return mol;
}

static void _determine_ranges_weights (Interpolate* inter, void* grid, Molecules* mol, int _max_sigma)
{
  int num_types = mol->num_types;
  inter->num_types = num_types;

  inter->range   = (int**) malloc (sizeof (int*) * num_types);
  memset (inter->range, 0, sizeof (int*) * num_types);

  inter->num_weights = (int*) malloc (sizeof (int) * num_types);
  memset(inter->num_weights, 0, sizeof (int) * num_types);

  inter->offsets = (int**) malloc (sizeof (int*) * 3 * num_types);
  memset (inter->offsets, 0, sizeof(int*) * 3 * num_types);

  inter->weights = (double**) malloc (sizeof (double*) * num_types);
  memset (inter->weights, 0, sizeof (double*) * num_types);

  inter->max_sigma = _max_sigma;
  double** basis = get_grid_basis (grid);
  float delta[3];
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  for (int i = 0; i < num_types; ++i)
    {
      float rad = mol->radius[i];
      float cut_off = rad;
      cut_off = inter->max_sigma * cut_off;
      inter->range[i] = realloc (inter->range[i], 3*sizeof (int));
      int* range = inter->range[i];
      range[0] = (int) ceil (cut_off/delta[0]);
      range[1] = (int) ceil (cut_off/delta[1]);
      range[2] = (int) ceil (cut_off/delta[2]);

      for (int x = -range[0]; x < range[0]; ++x)
        {
 	  float dx = x*delta[0];
          for (int y = -range[1]; y < range[1]; ++y)
            {
	      float dy = y*delta[1];
              for (int z = -range[2]; z < range[2]; ++z)
	        {
		  float dz = z*delta[2];
		  float dr2 = dx*dx + dy*dy + dz*dz;

                  if(dr2 < cut_off*cut_off)
                    {
                      int num = ++inter->num_weights[i];
                      inter->weights[i] = (double*) realloc (inter->weights[i], sizeof (double) * num); 
                      float rad2 = rad * rad;                          
		      inter->weights[i][num-1] = exp(-0.5*dr2/rad2)/((rad2*rad*2.*MY_PI*SQRT2*SQRT_MY_PI)); 
                      //1/(sigma*sqrt(2*pi))*exp(-0.5*x**2/sigma**2) * dx^3
                      inter->offsets[i] = (int*) realloc (inter->offsets[i], sizeof (int) * 3 * num);
                      inter->offsets[i][3*(num-1)+0] = x;
                      inter->offsets[i][3*(num-1)+1] = y;
                      inter->offsets[i][3*(num-1)+2] = z;
                    }
	        }
            }
        }
      
      int num = inter->num_weights[i];
      double sum = 0.;
      for (int w = 0; w < num; ++w)
          sum += inter->weights[i][w];
      /*for (int w = 0; w < num; ++w)
        {
          fprintf(stdout, "%lf %lf\n", sum, inter->weights[i][w]);
        }*/
      //fprintf(stderr, "%lf\n", sum*delta[0]*delta[1]*delta[2]);
      sum = 1./sum;
      for (int w = 0; w < num; ++w)
          inter->weights[i][w] *= sum;
    }
}

static void _determine_wca_ranges_weights (Interpolate* inter, void* grid, Molecules* mol)
{
  int num_types = mol->num_types;
  inter->num_types = num_types;

  inter->range   = (int**) malloc (sizeof (int*) * num_types);
  memset (inter->range, 0, sizeof (int*) * num_types);

  inter->num_weights = (int*) malloc (sizeof (int) * num_types);
  memset(inter->num_weights, 0, sizeof (int) * num_types);

  inter->offsets = (int**) malloc (sizeof (int*) * 3 * num_types);
  memset (inter->offsets, 0, sizeof(int*) * 3 * num_types);

  inter->weights = (double**) malloc (sizeof (double*) * num_types);
  memset (inter->weights, 0, sizeof (double*) * num_types);

  inter->max_sigma = POW_TWO_1OVER6;
  double** basis = get_grid_basis (grid);
  float delta[3];
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  for (int i = 0; i < num_types; ++i)
    {
      float rad = mol->radius[i];
      float cut_off = rad;
      cut_off = inter->max_sigma * cut_off;
      inter->range[i] = realloc (inter->range[i], 3*sizeof (int));
      int* range = inter->range[i];
      range[0] = (int) ceil (cut_off/delta[0]);
      range[1] = (int) ceil (cut_off/delta[1]);
      range[2] = (int) ceil (cut_off/delta[2]);

      for (int x = -range[0]; x < range[0]; ++x)
        {
 	  float dx = (x+0.5)*delta[0];
          for (int y = -range[1]; y < range[1]; ++y)
            {
	      float dy = (y+0.5)*delta[1];
              for (int z = -range[2]; z < range[2]; ++z)
	        {
		  float dz = (z+0.5)*delta[2];
		  float dr2 = dx*dx + dy*dy + dz*dz;

                  if(dr2 < cut_off*cut_off)
                    {
                      int num = ++inter->num_weights[i];
                      inter->weights[i] = (double*) realloc (inter->weights[i], sizeof (double) * num); 
                      float rad2 = rad * rad;
                      dr2 = rad2/dr2;
                      dr2 = dr2*dr2*dr2;                          
		      inter->weights[i][num-1] = 4*0.5*(dr2*dr2-dr2);
                      //1/(sigma*sqrt(2*pi))*exp(-0.5*x**2/sigma**2) * dx^3
                      inter->offsets[i] = (int*) realloc (inter->offsets[i], sizeof (int) * 3 * num);
                      inter->offsets[i][3*(num-1)+0] = x;
                      inter->offsets[i][3*(num-1)+1] = y;
                      inter->offsets[i][3*(num-1)+2] = z;
                    }
	        }
            }
        }
      int num = inter->num_weights[i];
      double sum = 0.;
      for (int w = 0; w < num; ++w)
          sum += inter->weights[i][w];
      /*for (int w = 0; w < num; ++w)
        {
          fprintf(stderr, "%d %d %d %lf\n", inter->offsets[i][3*w+0], inter->offsets[i][3*w+1],
                  inter->offsets[i][3*w+2], inter->weights[i][w]);
        }*/
      //fprintf(stderr, "%lf\n", sum*delta[0]*delta[1]*delta[2]);
      sum = 1./sum;
      for (int w = 0; w < num; ++w)
          inter->weights[i][w] *= sum;
    }
}

static void _set_grid_geometry (double** basis, double* origin, double* cell, int Nx, int Ny, int Nz)
{
  if (!(cell[1] == 0 && cell[3] == 0 && cell[4] == 0))
    {
      fprintf(stderr, "No support for non-orthogonal box\n");
      exit(-1);   
    }
  origin[0] = -cell[0]*0.5;
  origin[1] = -cell[2]*0.5;
  origin[2] = -cell[5]*0.5;
  basis[0][0] = cell[0] / Nx;
  basis[1][1] = cell[2] / Ny;
  basis[2][2] = cell[5] / Nz;
}

static void* _create_grid (double* cell, int Nx, int Ny, int Nz)
{
  double origin[3];
  double** basis = NULL;
  basis = (double**) malloc (sizeof (double*) * 3);
  for (int i = 0; i < 3; ++i)
    {
      basis[i] = (double*) malloc (sizeof (double)*3);
      memset (basis[i], 0, sizeof (double)*3);
    }
  _set_grid_geometry (basis, origin, cell, Nx, Ny, Nz);    
  void* grid = create_new_grid((size_t)Nx, (size_t)Ny, (size_t)Nz, origin, basis);
  for (int i = 0; i < 3; ++i)
      free (basis[i]);
  free (basis);
  return grid;
}

static void _clean_interpolate (Interpolate* inter)
{ 
  for (int i = 0; i < inter->num_types; ++i)
    {
      free (inter->range[i]);
      free (inter->weights[i]);
      free (inter->offsets[i]);
    } 
  free (inter->range);
  free (inter->num_weights);
  free (inter->offsets);
}

void _measure_minmax (double* cell, double* origin, void* _handler, int* mol_ids, int num_mols, float* coords, double skin)
{
  double min_x = 1e6, min_y  =  1e6, min_z =  1e6;
  double max_x = -1e6, max_y = -1e6, max_z = -1e6;
  for (int i = 0; i < num_mols; ++i)
    {
      int mol_id = mol_ids[i];
      int num_atoms = cluster_handler_get_num_beads (_handler, mol_id);
      for (int id = 0; id < num_atoms; ++id)
        {
          int atom_id = cluster_handler_get_bead_id (_handler, mol_id, id);
          float pos[3];
          pos[0] = coords[3*atom_id+0];
          pos[1] = coords[3*atom_id+1];
          pos[2] = coords[3*atom_id+2];
          if (pos[0] < min_x)
              min_x = pos[0];
          else if (pos[0] > max_x)
              max_x = pos[0];
          if (pos[1] < min_y)
              min_y = pos[1];
          else if (pos[1] > max_y)
              max_y = pos[1];
          if (pos[2] < min_z)
              min_z = pos[2];
          else if (pos[2] > max_z)
              max_z = pos[2];
        } 
    } 
  origin[0] = min_x-skin;
  origin[1] = min_y-skin;
  origin[2] = min_z-skin;
  cell[0] = max_x-min_x+2*skin;
  cell[1] = 0.;
  cell[2] = max_y-min_y+2*skin;
  cell[3] = 0.;
  cell[4] = 0.;
  cell[5] = max_z-min_z+2*skin;
}

void* create_bitvolmap_sel (void* _handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, int Nx, int Ny, int Nz, double skin)
{
  double cell[6];
  double _origin[3];
  _measure_minmax (cell, _origin, _handler, mol_ids, num_mols, coords, skin);
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);

  double** basis = NULL;
  basis = (double**) malloc (sizeof (double*) * 3);
  for (int i = 0; i < 3; ++i)
    {
      basis[i] = (double*) malloc (sizeof (double)*3);
      memset (basis[i], 0, sizeof (double)*3);
    }
  basis[0][0] = cell[0]/Nx;
  basis[1][1] = cell[2]/Ny;
  basis[2][2] = cell[5]/Nz;
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);
  void* grid = create_new_grid (Nx, Ny, Nz, _origin, basis); 
  for (int i = 0; i < 3; ++i)
      free (basis[i]);
  free (basis);

  Molecules* mol = (Molecules*)_mol;
  //float* mass = mol->mass;
  int* type = mol->type;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  basis = get_grid_basis (grid);

  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_ranges_weights (inter, grid, mol, 1);

  for (int m = 0; m < num_mols; ++m)
    {
      int mol_id = mol_ids[m];
      int num_atoms = cluster_handler_get_num_beads (_handler, mol_id);
      
      for (int i = 0; i < num_atoms; ++i)
        {
          int atom_id = cluster_handler_get_bead_id (_handler, mol_id, i);
          int t = type[atom_id];
          //float r = radius[t];
          //float m = mass[t];
          float pos[3];
          pos[0] = coords[3*atom_id+0];
          pos[1] = coords[3*atom_id+1];
          pos[2] = coords[3*atom_id+2];
          int home[3];
          home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
          home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
          home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;
          //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);
          int num_weights = inter->num_weights[t];
          for(int w = 0; w < num_weights; ++w)
            {
              int index[3];
              index[0] = inter->offsets[t][3*w+0];
              index[1] = inter->offsets[t][3*w+1];
              index[2] = inter->offsets[t][3*w+2];

              index[0] += home[0];
              index[1] += home[1];
              index[2] += home[2];
   
              index[0] = (index[0] + Nx) % Nx;
              index[1] = (index[1] + Ny) % Ny;
              index[2] = (index[2] + Nz) % Nz;

              int id = index[2] + Nz * (index[1] + Ny * index[0]);
              //data[id] += inter->weights[t][w] * (double) m;
              int mask = (int)data[id];
              mask |= 1;
              data[id] = mask;
            }
        }
    }
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_fwriter(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
    
}

void* create_volmap_sel (void* _handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, int Nx, int Ny, int Nz, double skin, int _max_sigma)
{
  double cell[6];
  double _origin[3];
  _measure_minmax (cell, _origin, _handler, mol_ids, num_mols, coords, skin);
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);

  double** basis = NULL;
  basis = (double**) malloc (sizeof (double*) * 3);
  for (int i = 0; i < 3; ++i)
    {
      basis[i] = (double*) malloc (sizeof (double)*3);
      memset (basis[i], 0, sizeof (double)*3);
    }
  basis[0][0] = cell[0]/Nx;
  basis[1][1] = cell[2]/Ny;
  basis[2][2] = cell[5]/Nz;
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);
  void* grid = create_new_grid (Nx, Ny, Nz, _origin, basis); 
  for (int i = 0; i < 3; ++i)
      free (basis[i]);
  free (basis);

  Molecules* mol = (Molecules*)_mol;
  float* mass = mol->mass;
  int* type = mol->type;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  basis = get_grid_basis (grid);

  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_ranges_weights (inter, grid, mol, _max_sigma);

  for (int m = 0; m < num_mols; ++m)
    {
      int mol_id = mol_ids[m];
      int num_atoms = cluster_handler_get_num_beads (_handler, mol_id);
      
      for (int i = 0; i < num_atoms; ++i)
        {
          int atom_id = cluster_handler_get_bead_id (_handler, mol_id, i);
          int t = type[atom_id];
          //float r = radius[t];
          float m = mass[t];
          float pos[3];
          pos[0] = coords[3*atom_id+0];
          pos[1] = coords[3*atom_id+1];
          pos[2] = coords[3*atom_id+2];
          int home[3];
          home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
          home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
          home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;
          //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);
          int num_weights = inter->num_weights[t];
          for(int w = 0; w < num_weights; ++w)
            {
              int index[3];
              index[0] = inter->offsets[t][3*w+0];
              index[1] = inter->offsets[t][3*w+1];
              index[2] = inter->offsets[t][3*w+2];

              index[0] += home[0];
              index[1] += home[1];
              index[2] += home[2];
   
              index[0] = (index[0] + Nx) % Nx;
              index[1] = (index[1] + Ny) % Ny;
              index[2] = (index[2] + Nz) % Nz;

              int id = index[2] + Nz * (index[1] + Ny * index[0]);
              data[id] += inter->weights[t][w] * (double) m;
            }
        }
    }
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
    
}

void* create_phi_map_gaussian_sel (void* _handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, int Nx, int Ny, int Nz, double skin, int _max_sigma)
{
  double cell[6];
  double _origin[3];
  _measure_minmax (cell, _origin, _handler, mol_ids, num_mols, coords, skin);
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);

  double** basis = NULL;
  basis = (double**) malloc (sizeof (double*) * 3);
  for (int i = 0; i < 3; ++i)
    {
      basis[i] = (double*) malloc (sizeof (double)*3);
      memset (basis[i], 0, sizeof (double)*3);
    }
  basis[0][0] = cell[0]/Nx;
  basis[1][1] = cell[2]/Ny;
  basis[2][2] = cell[5]/Nz;
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);
  void* grid = create_new_grid (Nx, Ny, Nz, _origin, basis); 
  for (int i = 0; i < 3; ++i)
      free (basis[i]);
  free (basis);

  Molecules* mol = (Molecules*)_mol;
  float* radius = mol->radius;
  int* type = mol->type;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  basis = get_grid_basis (grid);

  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_ranges (inter, grid, mol, _max_sigma);

  for (int m = 0; m < num_mols; ++m)
    {
      int mol_id = mol_ids[m];
      int num_atoms = cluster_handler_get_num_beads (_handler, mol_id);
      
      for (int i = 0; i < num_atoms; ++i)
        {
          int atom_id = cluster_handler_get_bead_id (_handler, mol_id, i);
          int t = type[atom_id];
          float rad = radius[t];
          float rad2 = rad*rad;
          //float m = mass[t];
          float pos[3];
          pos[0] = coords[3*atom_id+0];
          pos[1] = coords[3*atom_id+1];
          pos[2] = coords[3*atom_id+2];
          int home[3];
          home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
          home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
          home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;
          //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);
          int num_weights = inter->num_weights[t];
          for(int w = 0; w < num_weights; ++w)
            {
              int index[3];
              index[0] = inter->offsets[t][3*w+0];
              index[1] = inter->offsets[t][3*w+1];
              index[2] = inter->offsets[t][3*w+2];
              float dx = index[0]*delta[0];
              float dy = index[1]*delta[1];
              float dz = index[2]*delta[2];

              float dr2 = dx*dx+dy*dy+dz*dz;
              float val = exp(-0.5*dr2/rad2)/((rad2*rad*2.*MY_PI*SQRT2*SQRT_MY_PI));

              index[0] += home[0];
              index[1] += home[1];
              index[2] += home[2];
   
              index[0] = (index[0] + Nx) % Nx;
              index[1] = (index[1] + Ny) % Ny;
              index[2] = (index[2] + Nz) % Nz;

              int id = index[2] + Nz * (index[1] + Ny * index[0]);
              data[id] += val;
            }
        }
    }
  #ifndef NORMALIZED
  _normalize_to_one (data, Nx, Ny, Nz);
  #endif
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
    
}

void* create_wca_volmap_sel (void* _handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, int Nx, int Ny, int Nz, double skin)
{
  double cell[6];
  double _origin[3];
  _measure_minmax (cell, _origin, _handler, mol_ids, num_mols, coords, skin);
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);

  double** basis = NULL;
  basis = (double**) malloc (sizeof (double*) * 3);
  for (int i = 0; i < 3; ++i)
    {
      basis[i] = (double*) malloc (sizeof (double)*3);
      memset (basis[i], 0, sizeof (double)*3);
    }
  basis[0][0] = cell[0]/Nx;
  basis[1][1] = cell[2]/Ny;
  basis[2][2] = cell[5]/Nz;
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);
  void* grid = create_new_grid (Nx, Ny, Nz, _origin, basis); 
  for (int i = 0; i < 3; ++i)
      free (basis[i]);
  free (basis);

  Molecules* mol = (Molecules*)_mol;
  //float* mass = mol->mass;
  int* type = mol->type;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  basis = get_grid_basis (grid);

  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];
  //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_wca_ranges_weights (inter, grid, mol);

  for (int m = 0; m < num_mols; ++m)
    {
      int mol_id = mol_ids[m];
      int num_atoms = cluster_handler_get_num_beads (_handler, mol_id);
      
      for (int i = 0; i < num_atoms; ++i)
        {
          int atom_id = cluster_handler_get_bead_id (_handler, mol_id, i);
          int t = type[atom_id];
          //float r = radius[t];
          //float m = mass[t];
          float pos[3];
          pos[0] = coords[3*atom_id+0];
          pos[1] = coords[3*atom_id+1];
          pos[2] = coords[3*atom_id+2];
          int home[3];
          home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
          home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
          home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;
          //printf ("%lf %lf %lf\n", delta[0], delta[1], delta[2]);
          int num_weights = inter->num_weights[t];
          for(int w = 0; w < num_weights; ++w)
            {
              int index[3];
              index[0] = inter->offsets[t][3*w+0];
              index[1] = inter->offsets[t][3*w+1];
              index[2] = inter->offsets[t][3*w+2];

              index[0] += home[0];
              index[1] += home[1];
              index[2] += home[2];
   
              index[0] = (index[0] + Nx) % Nx;
              index[1] = (index[1] + Ny) % Ny;
              index[2] = (index[2] + Nz) % Nz;

              int id = index[2] + Nz * (index[1] + Ny * index[0]);
              data[id] += inter->weights[t][w];
            }
        }
    }
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
    
}

void* create_volmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz, int _max_sigma)
{
  
  void* grid = _create_grid (cell, Nx, Ny, Nz); 
  Molecules* mol = (Molecules*)_mol;
  float* mass = mol->mass;
  int* type = mol->type;
  //float* radius = mol->radius;
  int num_atoms = mol->num;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  double** basis = get_grid_basis (grid);
  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];

  //printf("%lf %lf %lf\n", delta[0], delta[1], delta[2]);
  //printf("%lf %lf %lf\n", origin[0], origin[1], origin[2]);

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_ranges_weights (inter, grid, mol, _max_sigma);

  for (int i = 0; i < num_atoms; ++i)
    {
      int t = type[i];
      //float r = radius[t];
      float m = mass[t];
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];
      int home[3];
      home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
      home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
      home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;

      int num_weights = inter->num_weights[t];
      for(int w = 0; w < num_weights; ++w)
        {
          int index[3];
          index[0] = inter->offsets[t][3*w+0];
          index[1] = inter->offsets[t][3*w+1];
          index[2] = inter->offsets[t][3*w+2];

          index[0] += home[0];
          index[1] += home[1];
          index[2] += home[2];
   
          index[0] = (index[0] + Nx) % Nx;
          index[1] = (index[1] + Ny) % Ny;
          index[2] = (index[2] + Nz) % Nz;

          int id = index[2] + Nz * (index[1] + Ny * index[0]);
          data[id] += inter->weights[t][w] * (double) m;
        }
    }
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
  //grid_destroy (grid);
}


void* create_phi_map_gaussian (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz, int _max_sigma)
{
  void* grid = _create_grid (cell, Nx, Ny, Nz); 
  Molecules* mol = (Molecules*)_mol;
  //float* mass = mol->mass;
  int* type = mol->type;
  float* radius = mol->radius;
  int num_atoms = mol->num;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  double** basis = get_grid_basis (grid);
  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_ranges (inter, grid, mol, _max_sigma);

  for (int i = 0; i < num_atoms; ++i)
    {
      int t = type[i];
      float rad = radius[t];
      float rad2 = rad*rad;
      //float m = mass[t];
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];
      int home[3];
      home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
      home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
      home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;

      int num_weights = inter->num_weights[t];
      for(int w = 0; w < num_weights; ++w)
        {
          int index[3];
          index[0] = inter->offsets[t][3*w+0];
          index[1] = inter->offsets[t][3*w+1];
          index[2] = inter->offsets[t][3*w+2];

          float dx = index[0]*delta[0];
          float dy = index[1]*delta[1];
          float dz = index[2]*delta[2];

          float dr2 = dx*dx+dy*dy+dz*dz;
          float val = exp(-0.5*dr2/rad2)/((rad2*rad*2.*MY_PI*SQRT2*SQRT_MY_PI));

          index[0] += home[0];
          index[1] += home[1];
          index[2] += home[2];
   
          index[0] = (index[0] + Nx) % Nx;
          index[1] = (index[1] + Ny) % Ny;
          index[2] = (index[2] + Nz) % Nz;

          int id = index[2] + Nz * (index[1] + Ny * index[0]);
          //data[id] += inter->weights[t][w] * (double) m;
          data[id] += val;
        }
    }
  #ifndef NORMALIZED
  _normalize_to_one (data, Nx, Ny, Nz);
  #endif
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
  //grid_destroy (grid);
}

void* create_bitvolmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz)
{
  void* grid = _create_grid (cell, Nx, Ny, Nz); 
  Molecules* mol = (Molecules*)_mol;
  //float* mass = mol->mass;
  int* type = mol->type;
  //float* radius = mol->radius;
  int num_atoms = mol->num;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  double** basis = get_grid_basis (grid);
  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_ranges_weights (inter, grid, mol, 1);

  for (int i = 0; i < num_atoms; ++i)
    {
      int t = type[i];
      //float r = radius[t];
      //float m = mass[t];
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];
      int home[3];
      home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
      home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
      home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;

      int num_weights = inter->num_weights[t];
      for(int w = 0; w < num_weights; ++w)
        {
          int index[3];
          index[0] = inter->offsets[t][3*w+0];
          index[1] = inter->offsets[t][3*w+1];
          index[2] = inter->offsets[t][3*w+2];

          index[0] += home[0];
          index[1] += home[1];
          index[2] += home[2];
   
          index[0] = (index[0] + Nx) % Nx;
          index[1] = (index[1] + Ny) % Ny;
          index[2] = (index[2] + Nz) % Nz;

          int id = index[2] + Nz * (index[1] + Ny * index[0]);
          //data[id] += inter->weights[t][w] * (double) m;
          int mask = (int)data[id];
          mask |= 1;
          data[id] = (double)mask;
        }
    }
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
  //grid_destroy (grid);
}

void* create_wca_volmap (void* _mol, float* coords, double* cell, int Nx, int Ny, int Nz)
{
  void* grid = _create_grid (cell, Nx, Ny, Nz); 
  Molecules* mol = (Molecules*)_mol;
  //float* mass = mol->mass;
  int* type = mol->type;
  //float* radius = mol->radius;
  int num_atoms = mol->num;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  double** basis = get_grid_basis (grid);
  double delta[3]; 
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];

  Interpolate* inter = malloc (sizeof (Interpolate));
  memset(inter, 0, sizeof(Interpolate));
  _determine_wca_ranges_weights (inter, grid, mol);

  for (int i = 0; i < num_atoms; ++i)
    {
      int t = type[i];
      //float r = radius[t];
      //float m = mass[t];
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];
      int home[3];
      home[0] = ((int) floor ((pos[0]-origin[0]) / delta[0]) + Nx) % Nx;
      home[1] = ((int) floor ((pos[1]-origin[1]) / delta[1]) + Ny) % Ny; 
      home[2] = ((int) floor ((pos[2]-origin[2]) / delta[2]) + Nz) % Nz;

      int num_weights = inter->num_weights[t];
      for(int w = 0; w < num_weights; ++w)
        {
          int index[3];
          index[0] = inter->offsets[t][3*w+0];
          index[1] = inter->offsets[t][3*w+1];
          index[2] = inter->offsets[t][3*w+2];

          index[0] += home[0];
          index[1] += home[1];
          index[2] += home[2];
   
          index[0] = (index[0] + Nx) % Nx;
          index[1] = (index[1] + Ny) % Ny;
          index[2] = (index[2] + Nz) % Nz;

          int id = index[2] + Nz * (index[1] + Ny * index[0]);
          data[id] += inter->weights[t][w];
        }
    }
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  _clean_interpolate (inter);
  return grid;
  //grid_destroy (grid);
}

static void _create_link_cell (int* head, int* link_cell, float* coords, int num_atoms, double* origin, int* ncell, float rc)
{
  /* Reset the headers, head */
  int lxyz = ncell[0] * ncell[1] * ncell[2];  
  for (int c = 0; c < lxyz; c++) 
      head[c] = -1;  
  /* Scan atoms to construct headers, head, & linked lists, lscl */  
  for (int i = 0; i < num_atoms; i++) 
  {    
      /* Vector cell index to which this atom belongs */
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];
      pos[0] -= origin[0];
      pos[1] -= origin[1];
      pos[2] -= origin[2]; 
      int mc[3];
      mc[0] = pos[0] / rc;
      mc[1] = pos[1] / rc;
      mc[2] = pos[2] / rc;   
      if(mc[0] < 0 || mc[0] >= ncell[0] || 
         mc[1] < 0 || mc[1] >= ncell[1] || 
         mc[2] < 0 || mc[2] >= ncell[2])
          continue; 
      /* Translate the vector cell index, mc, to a scalar cell index */    
      int c = mc[2] + ncell[2] * (mc[1] + ncell[1] * mc[0]); 
      /* Link to the previous occupant (or EMPTY if you're the 1st) */    
      link_cell[i] = head[c];    
      /* The last one goes to the header */    
      head[c] = i;  
  }
}

void* create_radius_map(void* _mol, float* coords, double* cell, 
float cut_off, float rc, float probe, int Nx, int Ny, int Nz)
{
    void* grid = _create_grid (cell, Nx, Ny, Nz);
    Molecules* mol = (Molecules*)_mol;
    int num_atoms = mol->num;

    double* data = get_grid_data (grid);
    double* origin = get_grid_origin (grid);
    double** basis = get_grid_basis (grid);
    double delta[3];
    delta[0] = basis[0][0];
    delta[1] = basis[1][1];
    delta[2] = basis[2][2];

    //create link cell list
    int ncell[3];
    ncell[0] = ceil (cell[0] / rc);
    ncell[1] = ceil (cell[2] / rc);
    ncell[2] = ceil (cell[5] / rc);
    long long int num_cells = ncell[0]*ncell[1]*ncell[2];
    int* head = (int*) malloc (sizeof (int)*num_cells);
    int* link_cell = (int*) malloc (sizeof (int)*num_atoms);

    _create_link_cell (head, link_cell, coords, num_atoms, origin, ncell, rc);  
    //for each cell, determine the value of phi
    //printf ("%lf %lf %lf %lf %lf %lf %d %d %d\n", cell[0], cell[2], cell[5], origin[0], origin[1], origin[2], ncell[0], ncell[1], ncell[2]);
    int nneigh = ceil(cut_off/rc);
    omp_set_num_threads(16);
    #pragma omp parallel for
    for(long long int index = 0; index < Nx*Ny*Nz; ++index)
    {
        int ii =  index / Nz / Ny;
        int jj = (index / Nz)% Ny;
        int kk =  index % Nz;
        float max_r = 1e9;

        float pos[3];
        pos[0] = origin[0]+(ii+0.5)*delta[0];
        pos[1] = origin[1]+(jj+0.5)*delta[1];
        pos[2] = origin[2]+(kk+0.5)*delta[2];
        int i = (((int)floor ((pos[0]-origin[0])/rc))+ncell[0])%ncell[0];
        int j = (((int)floor ((pos[1]-origin[1])/rc))+ncell[1])%ncell[1];
        int k = (((int)floor ((pos[2]-origin[2])/rc))+ncell[2])%ncell[2];

        /*if(i < 0 || i >= ncell[0] || j < 0 || j >= ncell[1] || k < 0 || k >= ncell[2])
        {
            data[index] = 1;
            continue;
        }*/
        for (int dx = -nneigh; dx <= nneigh; ++dx)
        {
            for (int dy = -nneigh; dy <= nneigh; ++dy)
            {
                for (int dz = -nneigh; dz <= nneigh; ++dz)
                {
                    int cell_id[3];
                    cell_id[0] = (i+dx+ncell[0])%ncell[0];
                    cell_id[1] = (j+dy+ncell[1])%ncell[1];
                    cell_id[2] = (k+dz+ncell[2])%ncell[2];
                    /*if(cell_id[0] < 0 || cell_id[0] >= ncell[0] || 
                       cell_id[1] < 0 || cell_id[1] >= ncell[1] || 
                       cell_id[2] < 0 || cell_id[2] >= ncell[2])
                        continue;*/
                    int c = cell_id[2]+ncell[2]*(cell_id[1]+ncell[1]*cell_id[0]);
                    int atom_id = head[c];
                    while (atom_id != -1)
                    {
                        float r[3];
                        r[0] = coords[3*atom_id+0];
                        r[1] = coords[3*atom_id+1]; 
                        r[2] = coords[3*atom_id+2];
                        r[0] = (pos[0]-r[0]);
                        r[1] = (pos[1]-r[1]);
                        r[2] = (pos[2]-r[2]);
                        //apply periodic boundary condition
                        r[0] -= floor((r[0]/cell[0])+0.5)*cell[0];
                        r[1] -= floor((r[1]/cell[2])+0.5)*cell[2];
                        r[2] -= floor((r[2]/cell[5])+0.5)*cell[5];
                        int t = mol->type[atom_id];
                        float rad = mol->radius[t];

                        float dr2 = r[0]*r[0]+r[1]*r[1]+r[2]*r[2];
                        dr2 = sqrt(dr2)-rad;
                                
                        if (dr2 < max_r)
                            max_r = dr2;
                        if(max_r < 0) break;
                        atom_id = link_cell[atom_id];                      
                    }
                    if(max_r < 0) break;
                }
                if(max_r < 0) break;
            }
            if(max_r < 0) break;
        }
        if(max_r < 0) max_r = 0;
        long long int index = kk + Nz*(jj+Ny*ii);
        //data[index] = max_r; 
        if(max_r > probe)
            data[index] = 1;
        else
            data[index] = 0;
    }
    //only for debug purpose to write the results of volmap
    #ifdef write_dx
    static int counter = 0;
    char tmp_name[128];
    snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
    counter++;
    gridformat_writer(tmp_name, grid);
    //fprintf(stderr, "%lf\n", total_elements(grid));
    #endif
    free (head);
    free (link_cell);
    return grid;
}

void* create_phi_map (void* _mol, float* coords, double* cell, 
float rho, float rc, int Nx, int Ny, int Nz)
{  
  void* grid = _create_grid (cell, Nx, Ny, Nz);
  Molecules* mol = (Molecules*)_mol;
  int num_atoms = mol->num;

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  double** basis = get_grid_basis (grid);
  double delta[3];
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];

  //create link cell list
  int ncell[3];
  ncell[0] = ceil (cell[0] / rc);
  ncell[1] = ceil (cell[2] / rc);
  ncell[2] = ceil (cell[5] / rc);
  long int num_cells = ncell[0]*ncell[1]*ncell[2];
  int* head = (int*) malloc (sizeof (int)*num_cells);
  int* link_cell = (int*) malloc (sizeof (int)*num_atoms);

  _create_link_cell (head, link_cell, coords, num_atoms, origin, ncell, rc);  
  //for each cell, determine the value of phi
  //printf ("%lf %lf %lf %lf %lf %lf %d %d %d\n", cell[0], cell[2], cell[5], origin[0], origin[1], origin[2], ncell[0], ncell[1], ncell[2]);
  for (int ii = 0; ii < Nx; ++ii)
    {
      for (int jj = 0; jj < Ny; ++jj)
        {
          for (int kk = 0; kk < Nz; ++kk)
            {
              float pos[3];
              pos[0] = origin[0]+(ii+0.5)*delta[0];
              pos[1] = origin[1]+(jj+0.5)*delta[1];
              pos[2] = origin[2]+(kk+0.5)*delta[2];
              int i = ((int)floor ((pos[0]-origin[0])/rc)+ncell[0])%ncell[0];
              int j = ((int)floor ((pos[1]-origin[1])/rc)+ncell[1])%ncell[1];
              int k = ((int)floor ((pos[2]-origin[2])/rc)+ncell[2])%ncell[2];
              
              float phi = 0.f;
              for (int dx = -1; dx <= 1; ++dx)
                {
                  for (int dy = -1; dy <= 1; ++dy)
                    {
                      for (int dz = -1; dz <= 1; ++dz)
                        {
                          int cell_id[3];
                          cell_id[0] = (i+dx+ncell[0]) % ncell[0];
                          cell_id[1] = (j+dy+ncell[1]) % ncell[1];
                          cell_id[2] = (k+dz+ncell[2]) % ncell[2];

                          int c = cell_id[2]+ncell[2]*(cell_id[1]+ncell[1]*cell_id[0]);
                          int atom_id = head[c];
                          while (atom_id != -1)
                            {
                              float r[3];
                              r[0] = coords[3*atom_id+0];
                              r[1] = coords[3*atom_id+1]; 
                              r[2] = coords[3*atom_id+2];
                              //printf ("%d %d %d %d %d %d %f %f %f %f %f %f\n", i, j, k, cell_id[0], cell_id[1], cell_id[2], pos[0], pos[1], pos[2], r[0], r[1], r[2]);
                              r[0] = (pos[0]-r[0]);
                              r[1] = (pos[1]-r[1]);
                              r[2] = (pos[2]-r[2]);

                              r[0] -= (floor (r[0]/cell[0]+0.5))*cell[0];
                              r[1] -= (floor (r[1]/cell[2]+0.5))*cell[2];
                              r[2] -= (floor (r[2]/cell[5]+0.5))*cell[5];
                              float dr2 = r[0]*r[0]+r[1]*r[1]+r[2]*r[2];
                              if (dr2 < rc*rc)
                                {
                                  dr2 = sqrt (dr2);
                                  dr2 = dr2/rc;
                                  phi += (1.f+3.f*dr2)*(1.-dr2)*(1.-dr2)*(1.-dr2);
                                }
                              atom_id = link_cell[atom_id];
                            } 
                        }
                    }
                }
              phi *= 105.f/(16.f*MY_PI*rc*rc*rc)/rho;  
              //normalized to one of the largest elements
              
              //if (phi > 1.f)
              //    phi = 1.f;
              long long int index = kk + Nz*(jj+Ny*ii);
              data[index] = phi; 
            }
        }
    }
  #ifndef NORMALIZED
  _normalize_to_one (data, Nx, Ny, Nz);
  #endif
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  free (head);
  free (link_cell);
  return grid;
}

static void _create_link_cell_sel (void* handler, int* mol_ids, int num_mols, int* head, int* link_cell, 
float* coords, double* origin, int* ncell, float rc)
{
  /* Reset the headers, head */
  int lxyz = ncell[0] * ncell[1] * ncell[2];
  for (int c = 0; c < lxyz; c++)
      head[c] = -1;
  /* Scan atoms to construct headers, head, & linked lists, lscl */
  /*only scan the molecular id in mol_id*/
  for (int m = 0; m < num_mols; ++m)
    {
      int mol_id = mol_ids[m];
      int num_beads = cluster_handler_get_num_beads (handler, mol_id);
      
      for (int a = 0; a < num_beads; a++)
        {
          /* Vector cell index to which this atom belongs */
          int i = cluster_handler_get_bead_id (handler, mol_id, a);
          float pos[3];
          pos[0] = coords[3*i+0];
          pos[1] = coords[3*i+1];
          pos[2] = coords[3*i+2];
          pos[0] -= origin[0];
          pos[1] -= origin[1];
          pos[2] -= origin[2];
          int mc[3];
          mc[0] = pos[0] / rc;
          mc[1] = pos[1] / rc;
          mc[2] = pos[2] / rc;
          /* Translate the vector cell index, mc, to a scalar cell index */
          int c = mc[2] + ncell[2] * (mc[1] + ncell[1] * mc[0]);
          /* Link to the previous occupant (or EMPTY if you're the 1st) */
          link_cell[i] = head[c];
          /* The last one goes to the header */
          head[c] = i;
        }
    }
}

void* create_phi_map_sel (void* handler, int* mol_ids, int num_mols, void* _mol, 
float* coords, float rho, float rc, int Nx, int Ny, int Nz, float skin)
{  
  Molecules* mol = (Molecules*)_mol;
  int num_atoms = mol->num;

  //create link cell list
  double cell[6];
  double _origin[3];
  //TODO measure min-max here
  _measure_minmax (cell, _origin, handler, mol_ids, num_mols, coords, skin);

  double** basis = NULL;
  basis = (double**) malloc (sizeof (double*) * 3);
  for (int i = 0; i < 3; ++i)
    {
      basis[i] = (double*) malloc (sizeof (double)*3);
      memset (basis[i], 0, sizeof (double)*3);
    }
  basis[0][0] = cell[0]/Nx;
  basis[1][1] = cell[2]/Ny;
  basis[2][2] = cell[5]/Nz;
  //printf ("%lf %lf %lf\n", cell[0], cell[2], cell[5]);
  void* grid = create_new_grid (Nx, Ny, Nz, _origin, basis);
  for (int i = 0; i < 3; ++i)
      free (basis[i]);
  free (basis);

  double* data = get_grid_data (grid);
  double* origin = get_grid_origin (grid);
  basis = get_grid_basis (grid);

  double delta[3];
  delta[0] = basis[0][0];
  delta[1] = basis[1][1];
  delta[2] = basis[2][2];

  int ncell[3];
  ncell[0] = ceil (cell[0] / rc);
  ncell[1] = ceil (cell[2] / rc);
  ncell[2] = ceil (cell[5] / rc);
  long int num_cells = ncell[0]*ncell[1]*ncell[2];
  int* head = (int*) malloc (sizeof (int)*num_cells);
  int* link_cell = (int*) malloc (sizeof (int)*num_atoms);

  _create_link_cell_sel (handler, mol_ids, num_mols, head, link_cell, coords, origin, ncell, rc);  
  //for each cell, determine the value of phi
  //printf ("%lf %lf %lf %lf %lf %lf %d %d %d\n", cell[0], cell[2], cell[5], origin[0], origin[1], origin[2], ncell[0], ncell[1], ncell[2]);
  for (int ii = 0; ii < Nx; ++ii)
    {
      for (int jj = 0; jj < Ny; ++jj)
        {
          for (int kk = 0; kk < Nz; ++kk)
            {
              float pos[3];
              pos[0] = origin[0]+(ii+0.5)*delta[0];
              pos[1] = origin[1]+(jj+0.5)*delta[1];
              pos[2] = origin[2]+(kk+0.5)*delta[2];
              int i = ((int)floor ((pos[0]-origin[0])/rc)+ncell[0])%ncell[0];
              int j = ((int)floor ((pos[1]-origin[1])/rc)+ncell[1])%ncell[1];
              int k = ((int)floor ((pos[2]-origin[2])/rc)+ncell[2])%ncell[2];
              
              float phi = 0.f;
              for (int dx = -1; dx <= 1; ++dx)
                {
                  for (int dy = -1; dy <= 1; ++dy)
                    {
                      for (int dz = -1; dz <= 1; ++dz)
                        {
                          int cell_id[3];
                          cell_id[0] = (i+dx+ncell[0]) % ncell[0];
                          cell_id[1] = (j+dy+ncell[1]) % ncell[1];
                          cell_id[2] = (k+dz+ncell[2]) % ncell[2];

                          int c = cell_id[2]+ncell[2]*(cell_id[1]+ncell[1]*cell_id[0]);
                          int atom_id = head[c];
                          while (atom_id != -1)
                            {
                              float r[3];
                              r[0] = coords[3*atom_id+0];
                              r[1] = coords[3*atom_id+1]; 
                              r[2] = coords[3*atom_id+2];
                              //printf ("%d %d %d %d %d %d %f %f %f %f %f %f\n", i, j, k, cell_id[0], cell_id[1], cell_id[2], pos[0], pos[1], pos[2], r[0], r[1], r[2]);
                              r[0] = (pos[0]-r[0]);
                              r[1] = (pos[1]-r[1]);
                              r[2] = (pos[2]-r[2]);

                              r[0] -= (floor (r[0]/cell[0]+0.5))*cell[0];
                              r[1] -= (floor (r[1]/cell[2]+0.5))*cell[2];
                              r[2] -= (floor (r[2]/cell[5]+0.5))*cell[5];
                              float dr2 = r[0]*r[0]+r[1]*r[1]+r[2]*r[2];
                              if (dr2 < rc*rc)
                                {
                                  dr2 = sqrt (dr2);
                                  dr2 = dr2/rc;
                                  phi += (1.f+3.f*dr2)*(1.-dr2)*(1.-dr2)*(1.-dr2);
                                }
                              atom_id = link_cell[atom_id];
                            } 
                        }
                    }
                }
              phi *= 105.f/(16.f*MY_PI*rc*rc*rc)/rho;  
              //normalized to one of the largest elements
              
              //if (phi > 1.f)
              //    phi = 1.f;
              long long int index = kk + Nz*(jj+Ny*ii);
              data[index] = phi; 
            }
        }
    }
  #ifndef NORMALIZED
  _normalize_to_one (data, Nx, Ny, Nz);
  #endif
  //only for debug purpose to write the results of volmap
  #ifdef write_dx
  static int counter = 0;
  char tmp_name[128];
  snprintf (tmp_name, sizeof (tmp_name), "%s%.5d%s", "test", counter, ".dx");
  counter++;
  gridformat_writer(tmp_name, grid);
  //fprintf(stderr, "%lf\n", total_elements(grid));
  #endif
  free (head);
  free (link_cell);
  return grid;
}

void clean_molecules (void* _mol)
{
  Molecules* mol = (Molecules*)_mol; 
  free (mol->mass);
  free (mol->radius);
  free (mol->type);
}
