#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "util.h"
#include "cluster_analysis.h"
typedef struct Beads
{
  int  num_types;
  float* mass;
  //float* radius;
} Beads;

typedef struct Topology
{
  int*  num_beads;
  int** bond_beads;
} Topology;

typedef struct Molecules
{
  int   num_mols;
  int*  mol_types;
  int*  num_mol_beads;
  int** mol_beads;
  int** mol_beads_types;
  float* total_mol_mass;

  Topology* topology;
  Beads* beads;

  int  num_beads;
  float* coords;
  int* beads_types;
  float* center_of_mass;
  double basis[6];
  double inv_basis[6];
} Molecules;

typedef struct Cluster
{
  int capacity;
  int num_mols;
  int* molId;
  struct Cluster* next;    
} Cluster;


typedef struct ClusterHandler
{
  Molecules* molecules;
  int num_clusters;
  Cluster* head;

} ClusterHandler;


void* create_cluster_handler()
{
  ClusterHandler* handler = (ClusterHandler*)malloc(sizeof(ClusterHandler));
  memset(handler, 0, sizeof(ClusterHandler));
  handler->molecules = (Molecules*)malloc(sizeof(Molecules));
  memset(handler->molecules, 0, sizeof(Molecules));
  handler->head = (Cluster*)malloc(sizeof(Cluster));
  memset(handler->head, 0, sizeof(Cluster));
  return handler;
}

static void _read_beads(ClusterHandler* handler, FILE* fpr)
{
  Beads* beads = (Beads*)malloc(sizeof(Beads));
  char* line = NULL;
  size_t nbytes = 0;

  getline(&line, &nbytes, fpr);
  trim_both_sides(line);
  beads->num_types = atoi(line);
  beads->mass = (float*)malloc(sizeof(float)*beads->num_types);
  //beads->radius = (float*)malloc(sizeof(float)*beads->num_types);

  memset(beads->mass, 0, sizeof(float)*beads->num_types);
  //memset(beads->radius, 0, sizeof(float)*beads->num_types);

  char** argv = NULL;
  int argc = 0;
  for(int b = 0; b < beads->num_types; ++b)
    {
      getline(&line, &nbytes, fpr);
      trim_both_sides(line);
      extract_arguments(&argv, &argc, line);

      if(argc != 2)
        {
            fprintf(stderr, "error arguments"); 
            exit(-1);
        }
      int n = atoi(argv[0]);
      beads->mass[n] = atof(argv[1]);
      //if(argc > 2)
          //beads->radius[n] = atof(argv[2]);
    }
  free(line);
  for(int i = 0; i < argc; ++i)
    free(argv[i]);
  free(argv);
  handler->molecules->beads = beads;
}

static void _compute_each_mol_mass(Molecules* mol)
{
  int num_mols = mol->num_mols;
  float* mass = mol->beads->mass;
    
  for(int i = 0; i < num_mols; ++i)
    {
      int* mol_atom_t  = mol->mol_beads_types[i];
      int num_atoms = mol->num_mol_beads[i];

      double sum = 0.;
      for(int b = 0; b < num_atoms; ++b)
        {
          int t = mol_atom_t[b];
          sum += (double)mass[t];
        }
      mol->total_mol_mass[i] = sum;
      #ifdef debug
      //fprintf(stderr, "total mass %f\n", sum);
      #endif
    }
}

static void _read_molecules(ClusterHandler* handler, FILE* fpr)
{
  Molecules* mol = handler->molecules;

  char* line = NULL;
  size_t nbytes = 0;

  getline(&line, &nbytes, fpr);
  trim_both_sides(line);
  mol->num_mols  = atoi(line);
  //printf("%d\n", mol->num_mols);
  mol->mol_beads = (int**)malloc(sizeof(int*)*mol->num_mols);
  mol->mol_beads_types = (int**)malloc(sizeof(int*)*mol->num_mols);
  mol->mol_types = (int*)malloc(sizeof(int)*mol->num_mols);
  mol->num_mol_beads = (int*)malloc(sizeof(int)*mol->num_mols);
  mol->total_mol_mass = (float*)malloc(sizeof(float)*mol->num_mols);
  memset(mol->total_mol_mass, 0, sizeof(float)*mol->num_mols);
  mol->center_of_mass = (float*)malloc(sizeof(float)*3*mol->num_mols);

  int count = 0;
  char** argv = NULL;
  int argc = 0;
  while(count < mol->num_mols)
    {
      getline(&line, &nbytes, fpr);
      trim_both_sides(line);
      if(strlen(line) == 0)
          continue;
      extract_arguments(&argv, &argc, line);
      int mol_id = atoi(argv[0]);
      int mol_type = atoi(argv[1]);
      int num_atoms = atoi(argv[2]);
      mol->num_mol_beads[mol_id] = num_atoms;
      mol->mol_types[mol_id] = mol_type;
      mol->mol_beads[mol_id] = (int*)malloc(sizeof(int)*num_atoms);
      mol->mol_beads_types[mol_id] = (int*)malloc(sizeof(int)*num_atoms);
      int num_atoms_read = 0;  
      while(num_atoms_read < num_atoms)
        {
          getline(&line, &nbytes, fpr);
          trim_both_sides(line);
          if(strlen(line) == 0)
            continue;
          extract_arguments(&argv, &argc, line);
          if(argc % 2 != 0)
            {
              fprintf(stderr, "error arguments, %s %d\n", __FILE__, __LINE__);
              exit(-1);
            }
          if((num_atoms_read + argc/2) > num_atoms)
            {
              fprintf(stderr, "error arguments, %s %d\n", __FILE__, __LINE__);
              exit(-1);
            }
          else
            {
              for(int i = 0; i < argc; i+=2)
                {
                  mol->mol_beads[mol_id][num_atoms_read] = atoi(argv[i]);
                  mol->mol_beads_types[mol_id][num_atoms_read] = atoi(argv[i+1]);
                  num_atoms_read++; 
                }
            }
        }
      count++;
    }
  count = 0;
  for(int i = 0; i < mol->num_mols; ++i)
      count += mol->num_mol_beads[i];
  mol->num_beads = count;
  mol->beads_types=malloc(sizeof(int)*count);
  for(int i = 0; i < count; ++i)
    mol->beads_types[i] = -1; 
  for(int i = 0; i < mol->num_mols; ++i)
  {
    int num_beads = mol->num_mol_beads[i];
    for(int j = 0; j < num_beads; ++j)
    {
      mol->beads_types[mol->mol_beads[i][j]] = mol->mol_beads_types[i][j];
    }
  } 
  for(int i = 0; i < count; ++i)
  {
    if(mol->beads_types[i] < 0)
    {
        fprintf(stderr, "error setting %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
  }
  _compute_each_mol_mass(mol);
  //mol->coords = (float*)malloc(sizeof(float)*3*mol->num_beads);
  free(line);
  for(int i = 0; i < argc; ++i)
    free(argv[i]);
  free(argv);
}

//end here 
//TODO: fix a potential bug to end the read in the while loop
static void _read_bonds(ClusterHandler* handler, FILE* fpr)
{
    Topology* topo = (Topology*)malloc(sizeof(Topology));
    Molecules* mol = handler->molecules;
    if(mol->num_beads == 0)
    {
        fprintf(stderr, "initialization error %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    topo->num_beads = (int*)malloc(sizeof(int)*mol->num_beads);
    memset(topo->num_beads, 0, sizeof(int)*mol->num_beads);
    topo->bond_beads = (int**)malloc(sizeof(int*)*mol->num_beads);
    memset(topo->bond_beads, 0, sizeof(int*)*mol->num_beads);
    char** argv = NULL;
    int argc = 0;
    char* line = NULL;
    size_t nbytes = 0;
    while(getline(&line, &nbytes, fpr)>=0)
    {
        trim_both_sides(line);
        if(strlen(line) == 0)
            continue;
        extract_arguments(&argv, &argc, line);
        int first = atoi(argv[0]);
        int old_num = topo->num_beads[first];
        topo->bond_beads[first] = realloc(topo->bond_beads[first], (old_num+argc-1)*sizeof(int));
        for(int i = 1; i < argc; ++i)
            topo->bond_beads[first][i-1+old_num] = atoi(argv[i]);
        topo->num_beads[first]+=argc-1;      
    }
    free(line);
    for(int i = 0; i < argc; ++i)
        free(argv[i]);
    free(argv);
    mol->topology = topo;
}

/*
void set_coordinates(void* _hander, float* coords)
{
    ClusterHandler
}
*/
void read_molecules_from_file(void* _handler, char* filename)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    
    FILE* fpr = fopen(filename, "rt");
    if(fpr == NULL)
    {
        fprintf(stderr, "error %s %d\n", __FILE__, __LINE__);
        exit(-1);
    } 
    char* line = NULL;
    size_t nbytes = 0;
    char** argv = NULL;
    int argc = 0;

    while(getline(&line, &nbytes, fpr)>0)
    {
        trim_both_sides(line);
        if(strlen(line) == 0)
            continue;
        extract_arguments(&argv, &argc, line);
        if(!strcmp(argv[0], "Beads"))
            _read_beads(handler, fpr);
        else if(!strcmp(argv[0], "Molecules"))
            _read_molecules(handler, fpr);
        else if(!strcmp(argv[0], "Bonds"))
            _read_bonds(handler, fpr);
        else
            continue;
    }
    for(int i = 0; i < argc; ++i)
        free(argv[i]);
    free(argv);
    free(line);
    fclose(fpr);
}
#if 0
#ifdef debug
void show_molecules(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules;
    printf("num moleclues %d\n", mol->num_mols);
    printf("mol types\n");
    for(int i = 0; i < mol->num_mols; ++i)
        printf("%d\n", mol->mol_types[i]);
    printf("num molecules beads\n");
    for(int i = 0; i < mol->num_mols; ++i)
        printf("%d\n", mol->num_mol_beads[i]);
    for(int i = 0; i < mol->num_mols; ++i)
    {
        for(int j = 0; j < mol->num_mol_beads[i]; ++j)
        {
            printf("%d %d\n", mol->mol_beads[i][j], mol->mol_beads_types[i][j]);
        }
    }
    Topology* topo = mol->topology;
    for(int i = 0; i < mol->num_beads; ++i)
    {
        for(int j = 0; j < topo->num_beads[i]; ++j)
        {
            printf("%d %d\n", i, topo->bond_beads[i][j]);
        }
    }
}
#endif
#endif
void cluster_handler_compute_center_of_mass(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules; 
    float* mass = mol->beads->mass;
    float* coords = mol->coords;
    float* center_of_mass = mol->center_of_mass;
    for(int m = 0; m < mol->num_mols; ++m)
    {
        int* atom_id = mol->mol_beads[m];
        int* atom_t  = mol->mol_beads_types[m];
        int  num_atoms = mol->num_mol_beads[m];
        double com[3] = {0., 0.,0.};
        double total_mass = 0.;
        for(int i = 0; i < num_atoms; ++i)
        {
            float m = mass[atom_t[i]];
            int id = atom_id[i];
            total_mass += (double)m;
            float x = coords[3*id+0];
            float y = coords[3*id+1];
            float z = coords[3*id+2];
            com[0] += (double)x*m;
            com[1] += (double)y*m;
            com[2] += (double)z*m;       
        }
        com[0] /= total_mass;
        com[1] /= total_mass;
        com[2] /= total_mass;
        center_of_mass[3*m+0] = (float)com[0];
        center_of_mass[3*m+1] = (float)com[1];
        center_of_mass[3*m+2] = (float)com[2];
    }
}

static int _wrap_vec_diff(float* diff, float* v1, float* v2, double* basis, double* inv_basis)
{
    int frac[3];
    diff[0] = v2[0]-v1[0];
    diff[1] = v2[1]-v1[1];
    diff[2] = v2[2]-v1[2];
    frac[0]  = (int) floor(inv_basis[0]*diff[0]+inv_basis[5]*diff[1]+inv_basis[4]*diff[2]+0.5);
    frac[1]  = (int) floor(inv_basis[1]*diff[1]+inv_basis[3]*diff[2]+0.5);
    frac[2]  = (int) floor(inv_basis[2]*diff[2]+0.5); 
    if(!(frac[0] == 0 && frac[1] == 0 && frac[2] == 0))
    {
        diff[0] -= basis[0]*frac[0]+basis[5]*frac[1]+basis[4]*frac[2];
        diff[1] -= basis[1]*frac[1]+basis[3]*frac[2];
        diff[2] -= basis[2]*frac[2];
        return 0;
    }
    return 1;
}

static float _wrap_diff2(float* v1, float* v2, double* basis, double* inv_basis)
{
    float diff[3];
    _wrap_vec_diff(diff, v1, v2, basis, inv_basis);
    return (diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2]);    
}

void join_molecules(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules;
    int** bond_beads = mol->topology->bond_beads;
    int* num_beads   = mol->topology->num_beads;
    int  num_atoms   = mol->num_beads;
    float* coords    = mol->coords;
    double* basis = mol->basis;
    double* inv_basis = mol->inv_basis;

    for(int i = 0; i < num_atoms; ++i)
    {
        int id1 = i;
        float pos1[3];
        pos1[0] = coords[3*id1+0];
        pos1[1] = coords[3*id1+1];
        pos1[2] = coords[3*id1+2];
        for(int j = 0; j < num_beads[i]; ++j)
        {
            int id2 = bond_beads[i][j];
            float pos2[3];
            pos2[0] = coords[3*id2+0];
            pos2[1] = coords[3*id2+1];
            pos2[2] = coords[3*id2+2]; 
            float diff[3];
            _wrap_vec_diff(diff, pos1, pos2, basis, inv_basis);
            pos2[0] = pos1[0]+diff[0];
            pos2[1] = pos1[1]+diff[1];
            pos2[2] = pos1[2]+diff[2];

            coords[3*id2+0] = pos2[0]; 
            coords[3*id2+1] = pos2[1];
            coords[3*id2+2] = pos2[2];
        }
    }
}

static void _add_one_molecule(Cluster* cluster, int mol_id)
{
    if((cluster->capacity)<=(cluster->num_mols))
    {
        cluster->capacity += 16;
        cluster->molId = (int*)realloc(cluster->molId, sizeof(int)*(cluster->capacity));
    }
    cluster->molId[cluster->num_mols] = mol_id;
    cluster->num_mols += 1;
}

static Cluster* _create_new_cluster()
{
    Cluster* cluster = (Cluster*)malloc(sizeof(Cluster));
    cluster->capacity = 16;
    cluster->num_mols = 0;
    cluster->molId = (int*)malloc(sizeof(int)*(cluster->capacity));
    cluster->next = NULL;
    return cluster;
}

static void _append_new_cluster(Cluster* head, Cluster* cluster)
{
    Cluster* ptr = head;
    while(ptr->next != NULL)
        ptr = ptr->next;
    ptr->next = cluster;
}

static void _find_my_cluster(ClusterHandler* handler, int mol_id, float cut_off)
{
    int is_found = 0;
    Cluster* ptr = handler->head;
    Molecules* mol = handler->molecules;
    double* basis = mol->basis;
    double* inv_basis = mol->inv_basis;
    float* com = mol->center_of_mass;

    float x[3];
    x[0] = com[3*mol_id+0];
    x[1] = com[3*mol_id+1];
    x[2] = com[3*mol_id+2];
    //printf("%d %f %f %f\n", mol_id, x[0], x[1], x[2]);
    while(ptr->next != NULL)
    {
        ptr = ptr->next;
        int num_mols = ptr->num_mols; 
        int* molId = ptr->molId;
        for(int i = 0; i < num_mols; ++i)
        {
            int id = molId[i];
            float y[3];
            y[0] = com[3*id+0];
            y[1] = com[3*id+1];
            y[2] = com[3*id+2];
            //printf("%d %f %f %f\n", id, y[0], y[1], y[2]);
            if(_wrap_diff2(x,y,basis,inv_basis) < cut_off*cut_off)
            {
                is_found = 1;
                _add_one_molecule(ptr, mol_id);
                break;
            }
        }
        if(is_found)
           break; 
    }
    if(!is_found)
    {
        Cluster* cluster = _create_new_cluster();
        _add_one_molecule(cluster, mol_id);
        _append_new_cluster(ptr, cluster);
        handler->num_clusters++;
    }
}

static void _merge(Cluster* ptr1, Cluster* ptr2)
{
    int n1 = ptr1->num_mols;
    int n2 = ptr2->num_mols;
    if((n1+n2) >= (ptr1->capacity))
    {
        ptr1->capacity = n1+n2+64;
        ptr1->molId = (int*)realloc(ptr1->molId, sizeof(int)*(ptr1->capacity));
    }
    for(int i = n1; i < n1+n2; ++i)
    {
        ptr1->molId[i] = ptr2->molId[i-n1];
        ptr1->num_mols++;    
    }        
}

static void _clean_cluster(Cluster* cluster)
{
    free(cluster->molId);
    free(cluster);
}

void clean_clusters(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler; 
    if(handler->num_clusters > 0)
    {
        Cluster* ptr = handler->head->next;
        while(ptr != NULL)
        {
            Cluster* next = ptr->next;
            _clean_cluster(ptr);
            handler->num_clusters--;
            ptr = next;
        }
    }
    if(handler->num_clusters != 0)
    {
        fprintf(stderr, "error in the number\n");
        exit(-1);
    }
    handler->head->next = NULL;
}

void clean_cluster_handler(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules;
    free(mol->mol_types);
    free(mol->num_mol_beads);
    for(int i = 0; i < mol->num_mols; ++i)
    {
        free(mol->mol_beads[i]);
        free(mol->mol_beads_types[i]);
    }
    free(mol->mol_beads);
    free(mol->mol_beads_types);
    free(mol->topology->num_beads);
    for(int i = 0; i < mol->num_beads; ++i)
        free(mol->topology->bond_beads[i]);
    free(mol->topology->bond_beads);
    free(mol->topology);
    free(mol->beads->mass);
    //free(mol->beads->radius);

    free(mol->beads);
    free(mol->total_mol_mass);
    //free(mol->coords);
    free(mol->center_of_mass);
    free(mol);
    clean_clusters(_handler);   
    free(handler->head);
    free(handler);
}

static void _merge_clusters(ClusterHandler* handler, float cut_off)
{
    Cluster* ptr1 = handler->head;
    Cluster* ptr2;
    Molecules* mol = handler->molecules;
    double* basis = mol->basis;
    double* inv_basis = mol->inv_basis;
    float* com = mol->center_of_mass;

    while(ptr1->next != NULL)
    {
        ptr1 = ptr1->next;
        ptr2 = ptr1;

        while(ptr2->next != NULL)
        {
            //need to update the local pointer; very subtle
            int* mol_id1 = ptr1->molId;
            int  num_mol1 = ptr1->num_mols;

            Cluster* prev_cluster = ptr2;
            ptr2 = ptr2->next;
            int* mol_id2 = ptr2->molId;
            int num_mol2 = ptr2->num_mols;
            volatile int need_merge = 0;
            for(int i = 0; i < num_mol1; ++i)
            {
                if(need_merge)
                    break;
                float x[3];
                int id1 = mol_id1[i];
                x[0] = com[3*id1+0];
                x[1] = com[3*id1+1];
                x[2] = com[3*id1+2];
                for(int j = 0; j < num_mol2; ++j)
                {
                    float y[3];
                    int id2 = mol_id2[j];
                    y[0] = com[3*id2+0];
                    y[1] = com[3*id2+1];
                    y[2] = com[3*id2+2];
                    if(_wrap_diff2(x,y,basis, inv_basis) < cut_off*cut_off)
                    {
                        //merge ptr2 to ptr1
                        _merge(ptr1,ptr2);
                        handler->num_clusters--;
                        Cluster* ptr2_next = ptr2->next;
                        //delete ptr2
                        _clean_cluster(ptr2); 
                        ptr2 = prev_cluster;
                        ptr2->next = ptr2_next;
                        need_merge = 1;
                        break;
                    }
                }
            }           
        }
    }
}

void cluster_handler_set_traj(void* _handler, float* _coords, double* cell)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules;
    double* basis = mol->basis;
    double* inv_basis = mol->inv_basis;
    mol->coords = _coords;
    //memcpy(mol->coords, _coords, 3*sizeof(float)*(handler->molecules->num_beads));   
    //_compute_center_of_mass(handler);

    double lx, ly, lz, xy, xz, yz;
    lx = cell[0];
    xy = cell[2]*cell[1];
    xz = cell[5]*cell[3];
    ly = sqrt(cell[2]*cell[2]-xy*xy);
    yz = (cell[2]*cell[5]*cell[4]-xy*xz)/ly;
    lz = sqrt(cell[5]*cell[5]-xz*xz-yz*yz);
   
    basis[0] = lx;
    basis[1] = ly;
    basis[2] = ly;
    basis[3] = yz;
    basis[4] = xz;
    basis[5] = xy;
    //printf("%lf %lf %lf %lf %lf %lf\n", basis[0], basis[1], basis[2], basis[3], basis[4], basis[5]);  
    inv_basis[0] = 1./lx;
    inv_basis[1] = 1./ly;
    inv_basis[2] = 1./lz; 
    inv_basis[3] = -basis[3] / (basis[1]*basis[2]);
    inv_basis[4] = (basis[3]*basis[5] - basis[1]*basis[4]) / (basis[0]*basis[1]*basis[2]);
    inv_basis[5] = -basis[5] / (basis[0]*basis[1]);
}

void find_clusters(void* _handler, float cut_off)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules; 
    int num_mols = mol->num_mols;
    //Cluster* head = handler->head;

    for(int i = 0; i < num_mols; ++i)
        _find_my_cluster(handler, i, cut_off);       
    #if 0
    #ifdef debug
    show_cluster(handler);
    #endif
    #endif 
    _merge_clusters(handler, cut_off);
}

float* cluster_handler_get_center_of_mass(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules;
    return mol->center_of_mass;
}

/*int get_num_molecules(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Molecules* mol = handler->molecules;
    return mol->num_mols;
}*/

static void _arrange_cluster(Molecules* mol, Cluster* cluster)
{
    int num_mols = cluster->num_mols; 
    int* molId = cluster->molId;
    float* com = mol->center_of_mass;
    float* coords = mol->coords;
    double* basis = mol->basis;
    double* inv_basis = mol->inv_basis;
    //for(int i = 0; i < num_mols-1; ++i)
    for(int i = 0; i <= 0; ++i)
    {
        int mol_id1 = molId[i];
        float x[3];
        x[0] = com[3*mol_id1+0];
        x[1] = com[3*mol_id1+1];
        x[2] = com[3*mol_id1+2];
        for(int j = i+1; j < num_mols; ++j)
        {
            int mol_id2 = molId[j];
            float y[3];
            y[0] = com[3*mol_id2+0];
            y[1] = com[3*mol_id2+1];
            y[2] = com[3*mol_id2+2];
            float diff[3];
            //if two molecules are not in the same image box, move one of them
            if(!_wrap_vec_diff(diff, x, y, basis, inv_basis))
            {
                float shift[3];
                shift[0] = diff[0]+x[0];
                shift[1] = diff[1]+x[1];
                shift[2] = diff[2]+x[2];
 
                com[3*mol_id2+0] = shift[0];
                com[3*mol_id2+1] = shift[1];
                com[3*mol_id2+2] = shift[2];
                shift[0] -= y[0];
                shift[1] -= y[1];
                shift[2] -= y[2];
                int num_atoms = mol->num_mol_beads[mol_id2];
                int* mol_atom_id = mol->mol_beads[mol_id2];
                for(int k = 0; k < num_atoms; ++k)
                {
                    int atom_id = mol_atom_id[k];
                    float r[3];
                    r[0] = coords[3*atom_id+0];
                    r[1] = coords[3*atom_id+1];
                    r[2] = coords[3*atom_id+2];
                    r[0] += shift[0];
                    r[1] += shift[1];
                    r[2] += shift[2];
                    coords[3*atom_id+0] = r[0];
                    coords[3*atom_id+1] = r[1];
                    coords[3*atom_id+2] = r[2];
                }
            }
        }
    }
}

//shift all of the molecules in the cluster such that
//all of the molecules are in the same image box.
void arrange_all_clusters(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Cluster* ptr = handler->head->next;
    Molecules* mol = handler->molecules;
    while(ptr != NULL)
    {
        _arrange_cluster(mol, ptr);
        ptr = ptr->next;
    }
}

static Cluster* _find_largest_cluster(ClusterHandler* handler, int min_size)
{
    Cluster* ptr = handler->head->next;
    Cluster* cluster = NULL;
    int num_clusters = 0;
    while(ptr != NULL)
    {
        int current = ptr->num_mols;
        if(current >= min_size && current > num_clusters)
        {
            num_clusters = current;
            cluster = ptr;
        }
        ptr = ptr->next;
    }
    return cluster;    
}

static void _compute_center_of_mass_cluster(float* com, ClusterHandler* handler, Cluster* cluster)
{
    int* molId = cluster->molId;
    int num_mols = cluster->num_mols;
    Molecules* mol = handler->molecules;
    float* center_of_mass = mol->center_of_mass;
    double sum[3];
    sum[0] = 0.;
    sum[1] = 0.;
    sum[2] = 0.;
    double total_mass = 0.;
    for(int i = 0; i < num_mols; ++i)
    {
        int mol_id = molId[i];
        float mol_mass = mol->total_mol_mass[mol_id];
        float x[3]; 
        x[0] = center_of_mass[3*mol_id+0];
        x[1] = center_of_mass[3*mol_id+1];
        x[2] = center_of_mass[3*mol_id+2];
        x[0] *= mol_mass;
        x[1] *= mol_mass;
        x[2] *= mol_mass;
        sum[0] += (double)x[0];
        sum[1] += (double)x[1];
        sum[2] += (double)x[2]; 
        total_mass += (double)mol_mass;
    }
    com[0] = (float)(sum[0]/total_mass);
    com[1] = (float)(sum[1]/total_mass);
    com[2] = (float)(sum[2]/total_mass);
}

static void _shift_cluster(Molecules* mol, Cluster* cluster, float* shift)
{
    int num_mols = cluster->num_mols;
    int* molId = cluster->molId;
    float* center_of_mass = mol->center_of_mass;
    float* coords = mol->coords;
    for(int m = 0; m < num_mols; ++m)
    {
        int mol_id = molId[m];
        float com[3];
        com[0] = center_of_mass[3*mol_id+0];
        com[1] = center_of_mass[3*mol_id+1];
        com[2] = center_of_mass[3*mol_id+2];
        com[0] += shift[0];
        com[1] += shift[1];
        com[2] += shift[2];
        center_of_mass[3*mol_id+0] = com[0];
        center_of_mass[3*mol_id+1] = com[1];
        center_of_mass[3*mol_id+2] = com[2];
        int* atom_id = mol->mol_beads[mol_id];
        int  num_atoms = mol->num_mol_beads[mol_id];
        for(int i = 0; i < num_atoms; ++i)
        {
            int id = atom_id[i];
            float x[3];
            x[0] = coords[3*id+0];
            x[1] = coords[3*id+1];
            x[2] = coords[3*id+2];
            x[0] += shift[0];
            x[1] += shift[1];
            x[2] += shift[2];
            coords[3*id+0] = x[0];
            coords[3*id+1] = x[1];
            coords[3*id+2] = x[2];
        }       
    } 
}

static void _align_clusters(ClusterHandler* handler, float* center, Cluster* center_cluster)
{
    Molecules* mol = handler->molecules;
    Cluster* ptr = handler->head->next;
    double* basis = mol->basis;
    double* inv_basis = mol->inv_basis;
    while(ptr != NULL)
    {
        //if(ptr != center_cluster)
        
        float shift[3];
        float com[3];
        if(ptr != center_cluster)
        {
            _compute_center_of_mass_cluster(com, handler, ptr);
            _wrap_vec_diff(shift, center, com, basis, inv_basis);
        }
        else
        {
            shift[0] = 0.;
            shift[1] = 0.;
            shift[2] = 0.;
            com[0] = center[0];
            com[1] = center[1];
            com[2] = center[2];
        }
        shift[0] -= com[0];
        shift[1] -= com[1];
        shift[2] -= com[2];
        _shift_cluster(mol, ptr, shift);
        ptr = ptr->next;
    }
}

void align_clusters(void* _handler, int min_size)
{
    arrange_all_clusters(_handler);
    ClusterHandler* handler = (ClusterHandler*)_handler;
    Cluster* largest_cluster = _find_largest_cluster(handler, min_size);
    //fprintf(stderr, "done finding\n");
    float com[3];
    if(largest_cluster != NULL)
    {
        _compute_center_of_mass_cluster(com, handler, largest_cluster);
        _align_clusters(handler, com, largest_cluster);    
    }
}

static void _compute_center_of_mass_whole (float* center_of_mass, Molecules* mol)
{
  int num_mols = mol->num_mols;
  double sum[3] = {0., 0., 0.};
  double total_mass = 0.; 
  for (int m = 0; m < num_mols; ++m)
    {
      float com[3];
      com[0] = mol->center_of_mass[3*m+0];
      com[1] = mol->center_of_mass[3*m+1];
      com[2] = mol->center_of_mass[3*m+2];
      float mass = mol->total_mol_mass[m];
      sum[0] += (double) mass * (double) com[0];
      sum[1] += (double) mass * (double) com[1];
      sum[2] += (double) mass * (double) com[2];
      total_mass += (double) mass;
    }  
  center_of_mass[0] = (float) sum[0] / (float) total_mass;
  center_of_mass[1] = (float) sum[1] / (float) total_mass;
  center_of_mass[2] = (float) sum[2] / (float) total_mass;
}

//write a code to align whole system
void align_clusters_whole (void* _handler)
{
  arrange_all_clusters (_handler);
  ClusterHandler* handler = (ClusterHandler*) _handler;
  Molecules* mol = handler->molecules;
  float com[3];
  _compute_center_of_mass_whole (com, mol);
  com[0] = -com[0];
  com[1] = -com[1];
  com[2] = -com[2];
  Cluster* ptr = handler->head->next;
  while (ptr != NULL)
    {
      _shift_cluster (mol, ptr, com);
      ptr = ptr->next;
    } 
}

int 
cluster_handler_get_num_molecules (void* _handler)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;

  return handler->molecules->num_mols;    
}

int
cluster_handler_get_num_beads (void* _handler, int mol_id)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;
  return handler->molecules->num_mol_beads[mol_id];
}

float
cluster_handler_get_mass (void* _handler, int mol_id, int bead_index)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;
  Molecules* mol = handler->molecules;
  int t = mol->mol_beads_types[mol_id][bead_index];
  return mol->beads->mass[t];
}
/*
float 
cluster_handler_get_radius_index(void* _handler, int index)
{
    ClusterHandler* handler = (ClusterHandler*) _handler;
    Molecules* mol = handler->molecules;
    return mol->radius[mol->beads_types[bead_id]];
}

float 
cluster_handler_get_mass_index(void* _handler, int index)
{
    ClusterHandler* handler = (ClusterHandler*) _handler;
    Molecules* mol = handler->molecules;
    return mol->mass[mol->beads_types[bead_id]];
}
*/

int 
cluster_handler_get_bead_id (void* _handler, int mol_id, int bead_index)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;
  return handler->molecules->mol_beads[mol_id][bead_index]; 
}

int 
cluster_handler_get_type_id (void* _handler, int mol_id, int bead_index)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;
  Molecules* mol = handler->molecules;
  return mol->mol_beads_types[mol_id][bead_index];
}

int 
cluster_handler_get_atom_type_id (void* _handler, int bead_id)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;
  Molecules* mol = handler->molecules;
  return mol->beads_types[bead_id];
}

int
cluster_handler_get_num_types (void* _handler)
{
  ClusterHandler* handler = (ClusterHandler*) _handler;
  Molecules* mol = handler->molecules;
  return mol->beads->num_types;
}

//get total number of clusters in the system
int 
cluster_handler_get_num_clusters (void* _handler)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  return handler->num_clusters;
}

//get total number of clusters in the system above some threshold
int 
cluster_handler_get_num_clusters_above (void* _handler, int threshold)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr = handler->head->next;
  int count = 0;
  while (ptr != NULL)
    {
      if (ptr->num_mols >= threshold)
        {
          count++;
        }
      ptr = ptr->next;
    }
  return count;
}

//get number of clusters with a given number of molecules
int 
cluster_handler_get_num_clusters_molecules (void* _handler, int num_mols)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr = handler->head->next;
  int count = 0;
  while (ptr != NULL)
    {
      if (ptr->num_mols == num_mols)
        {
          count++;
        }
      ptr = ptr->next;
    }
  return count;
}

//get number of molecules in the largest clusters
int 
cluster_handler_get_num_molecules_in_largest_cluster (void* _handler, int threshold)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr;
  if ((ptr=_find_largest_cluster (handler, threshold)) != NULL)
    return ptr->num_mols;
  else
    return 0;
}

int
cluster_handler_get_num_atoms_in_largest_cluster (void* _handler, int threshold)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr;
  Molecules* mol = handler->molecules;
  if ((ptr=_find_largest_cluster (handler, threshold)) != NULL)
    {
      int* mol_id = ptr->molId;
      int count = 0;
      for (int i = 0; i < ptr->num_mols; ++i)
        {
          count += mol->num_mol_beads[mol_id[i]];
        }
      return count;       
    }
  else
    {
        return 0;
    }
}

//get total number of molecules in clusters above some threshold
int 
cluster_handler_get_num_molecules_in_clusters_above (void* _handler, int threshold)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr = handler->head->next;
  int count = 0;
  while (ptr != NULL)
    {
      if (ptr->num_mols >= threshold)
        {
          count += ptr->num_mols;
        }
      ptr = ptr->next;
    }
  return count;
}

//get an array of largest cluster IDs above some threshold
//you have to free the array afterwards
int*
cluster_handler_get_largest_cluster_id (void* _handler, int threshold)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr = handler->head->next;
  int num_mols_largest_cluster = cluster_handler_get_num_molecules_in_largest_cluster (_handler, threshold);
  if (num_mols_largest_cluster < 0)
    return NULL;
  int n = cluster_handler_get_num_clusters_molecules (_handler, num_mols_largest_cluster);
  int* id_list = malloc (sizeof (int)*n);
  int count = 0;
  int id = 0;
  while (ptr != NULL)
    {
      if (ptr->num_mols == num_mols_largest_cluster)
        {
          id_list[count] = id;
          count++;
        }
      ptr = ptr->next;
      id++;
    }
  return id_list;
}

//get an array of cluster IDs which contains num_mols molecules
//you has to free the array afterwards
int*
cluster_handler_get_clusters_id (void* _handler, int num_mols)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr = handler->head->next;
  int n = cluster_handler_get_num_clusters_molecules (_handler, num_mols);
  if (n == 0)
    return NULL;
  int* id_list = malloc (sizeof (int)*n);
  int count = 0;
  int id = 0;
  while (ptr != NULL)
    {
      if (ptr->num_mols == num_mols)
        {
          id_list[count] = id;
          count++;
        }
      ptr = ptr->next;
      id++;
    }
  return id_list;
}

//get a pointer to the cluster
void* 
cluster_handler_get_cluster (void* _handler, int cluster_id)
{
  ClusterHandler* handler = (ClusterHandler*)_handler;
  Cluster* ptr = handler->head->next;
  if (cluster_id >= handler->num_clusters)
    {
      return NULL;
    } 
  int count = 0;
  while (count != cluster_id)
    {
      ptr = ptr->next;
      count++;  
    }
  return ptr;
}

//get the molecule id array using cluster id
int*
cluster_handler_get_molecular_id (void* _cluster)
{
  return ((Cluster*)_cluster)->molId;
}
#if 0
#ifdef debug
void show_cluster(void* _handler)
{
    ClusterHandler* handler = (ClusterHandler*)_handler;
    fprintf(stdout, "%d clusters in the systems\n", handler->num_clusters);
    Cluster* ptr = handler->head->next;
    int c = 0;
    while(ptr != NULL)
    {
        int num_mols = ptr->num_mols;
        fprintf(stdout, "%d molecules in the cluster ID %d\n", num_mols, c);
        for(int i = 0; i < num_mols; ++i)
            fprintf(stdout, "mol id is %d\n", ptr->molId[i]);
        ptr = ptr->next;
        c++;
    }
}
#endif
#endif
/*
void shift_clusters(void* _handler)
{
}
*/
/*
void create_cluster_list()
{
}

void join_molecules()
{
}

void find_cluster()
{
}

void find_cluster_and_move()
{
}

void merge_cluster()
{
}
*/
