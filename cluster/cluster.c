#include <stdlib.h>
#include <stdio.h>
#include "cluster_analysis.h"
#include "traj_dcd_handler.h"
int main(int argc, char** argv)
{
    if(argc < 7)
        exit(-1);
    //create a cluster handler
    void* cluster_handler = create_cluster_handler();
    //initialize the handler
    read_molecules_from_file(cluster_handler, argv[1]);
    //create a trajectory handler
    void* traj_handler = create_traj_dcd_handler(argv[2]); 
    void* traj_writer  = create_traj_dcd_writer(argv[3]);

    //set header
    set_remark(traj_writer, "Created by Han-Yi Chou");
    set_time_step(traj_writer, 0);
    set_save_period(traj_writer, 1);
    set_num_atoms(traj_writer, get_num_atoms(traj_handler));
   
    int first_frame = atoi (argv[4]);
    int last_frame = atoi (argv[5]);
    int skip = atoi (argv[6]);
    int num_frames = (last_frame-first_frame)/skip;
    read_dcd(traj_handler, first_frame, last_frame, skip);
    for(int t = 0; t < num_frames; ++t)
    {
        float* coords = get_coords(traj_handler, t);
        double* cell  = get_unitcell(traj_handler, t);
        cluster_handler_set_traj(cluster_handler, coords, cell);
        join_molecules(cluster_handler);
        cluster_handler_compute_center_of_mass(cluster_handler);
        find_clusters(cluster_handler, 150);
        #ifdef debug
        show_cluster(cluster_handler);
        #endif
        //arrange_all_clusters(cluster_handler);
        align_clusters(cluster_handler, 3);
        write_data(traj_writer, coords, cell);
        clean_clusters(cluster_handler);
    }
    /*for(int t = 0; t < 100; ++t)
    {
        float* coords = get_coords(traj_handler, t);
        double* cell  = get_unitcell(traj_handler, t);
        set_traj(cluster_handler, coords, cell);
        join_molecules(cluster_handler);
        compute_center_of_mass(cluster_handler);
        //int num_mols = get_num_molecules(cluster_handler);
        //float* com = get_center_of_mass(cluster_handler);
    //printf("%lf %lf %lf %lf %lf %lf\n", cell[0], cell[1], cell[2], cell[3], cell[4], cell[5]);
    //for(int i = 0; i < num_mols; ++i)
        //printf("mol %d %f %f %f\n", i, com[3*i+0], com[3*i+1], com[3*i+2]);

        find_clusters(cluster_handler, 100);
        #ifdef debug
        show_cluster(cluster_handler); 
        #endif
        clean_clusters(cluster_handler);
    }*/
    /* 
    for(int i = 39998; i < 40001; ++i)
    {
        read_dcd(cluster_handler, i, i+1, 1);    
        float* coords = get_coords(traj_handler, i-39998);
        double* cell  = get_unitcell(traj_handler, i-39998);
        set_traj(cluster_handler, coords, cell);
        find_clusters(cluster_handler, atof(argv[3]));
        #ifdef debug
        show_molecules(cluster_handler);
        #endif 
        clean_clusters(cluster_handler);
    }*/
    clean_cluster_handler(cluster_handler);
    clean_traj_dcd_handler(traj_handler);
    clean_traj_dcd_writer(traj_writer);
    return 0;
}
