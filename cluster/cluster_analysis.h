#ifndef CLUSTER_ANALYSIS_H_
#define CLUSTER_ANALYSIS_H_
void* create_cluster_handler();
void read_molecules_from_file(void*, char*);
void clean_cluster_handler(void*);
void clean_clusters(void*);
void cluster_handler_set_traj(void*, float*, double*);
void find_clusters(void*, float);
void join_molecules(void*);
void cluster_handler_compute_center_of_mass(void*);
float* cluster_handler_get_center_of_mass(void*);
//int get_num_molecules(void*);
void align_clusters(void*, int);
void arrange_all_clusters(void*);
void align_clusters_whole (void*);
int cluster_handler_get_num_molecules (void*);
int cluster_handler_get_num_beads (void*, int);
float cluster_handler_get_mass (void*, int, int);
int cluster_handler_get_bead_id (void*, int, int);
int cluster_handler_get_type_id (void*, int,int);
int cluster_handler_get_atom_type_id (void*,int);
int cluster_handler_get_num_types (void*);

int cluster_handler_get_num_clusters (void*);
int cluster_handler_get_num_clusters_above (void*, int);
int cluster_handler_get_num_clusters_molecules (void*, int);
int cluster_handler_get_num_molecules_in_largest_cluster (void*, int);
int cluster_handler_get_num_molecules_in_clusters_above (void* _handler, int threshold);
int* cluster_handler_get_largest_cluster_id (void* _handler, int threshold);
int* cluster_handler_get_clusters_id (void* _handler, int num_mols);
void* cluster_handler_get_cluster (void*, int);
int* cluster_handler_get_molecular_id (void*);
int cluster_handler_get_num_atoms_in_largest_cluster (void* _handler, int threshold);


//float cluster_handler_get_radius_index(void* _handler, int index);
//float cluster_handler_get_mass_index(void* _handler, int index);

#if 0
#ifdef debug
void show_molecules(void*);
void show_cluster(void*);
#endif
#endif
#endif
