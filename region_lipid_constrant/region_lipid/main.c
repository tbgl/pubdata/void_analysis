#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "traj_dcd_handler.h"
//#include "volmap.h"
#include "util.h"
#include "grid_reader.h"
#include <omp.h>

static void make_pmf(void* new_grid, void* grid, float radius)
{
    size_t nx = get_nx(grid);
    size_t ny = get_ny(grid);
    size_t nz = get_nz(grid);
    size_t num = nx*ny*nz;

    double* origin = get_grid_origin(grid);
    double** basis = get_grid_basis(grid);

    double* data = get_grid_data(grid);
    double* new_data = get_grid_data(new_grid);

    #pragma omp parallel for
    for(size_t i = 0; i < num; ++i)
    {
        float pos[3];
        int id[3];
        id[0] = i / nz / ny;
        id[1] = i / nz % ny;
        id[2] = i % nz;
        pos[0] = (float)(origin[0]+basis[0][0]*(id[0]+0.5));
        pos[1] = (float)(origin[1]+basis[1][1]*(id[1]+0.5));
        pos[2] = (float)(origin[2]+basis[2][2]*(id[2]+0.5));
 
        if(data[i] > 0 && is_in_grid_size_periodic(grid, pos, radius))
        //if(data[i] > 1e-8)
            new_data[i] = 1;
        else
            new_data[i] = 0; 
    }
}

static void _extract_arguments(char* mol_name, float* radius,
                               char** argv, int argc)
{
    for (int i = 0; i < argc; ++i)
    {
        if (!strcmp (argv[i], "-name"))
        {
            strcpy(mol_name, argv[i+1]);
            i += 1;
        }
        else if (!strcmp (argv[i], "-radius"))
        {
            *radius = atof(argv[i+1]);
            i += 1;
        }
    }
}
 
int main(int argc, char** argv)
{
    char output_name[1024];
    strcpy(output_name, "region_");

    char mol_name[1024];
    float radius = 0;

    _extract_arguments(mol_name, &radius, argv, argc);
    void* input_region = gridformat_reader("lipid.dx");
    void* output = copy_grid(input_region);
    
    make_pmf(output, input_region, radius);
    //write out the data in DX file format
    strcat(output_name, mol_name);
    strcat(output_name, ".dx");
    gridformat_writer(output_name, output); 
    grid_destroy(input_region);
    grid_destroy(output);
    return 0;
}
