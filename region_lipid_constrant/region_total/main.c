#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "grid_reader.h"
#include <omp.h>
#if 0
static void make_statistics(float* coords, int num, void* grid)
{
    double* origin;
    double** basis;
    size_t nx, ny, nz;

    origin = get_grid_origin(grid);
    basis = get_grid_basis(grid);
    nx = get_nx(grid);
    ny = get_ny(grid);
    nz = get_nz(grid);
    double* data = get_grid_data(grid);

    #pragma omp parallel for
    for(int i = 0; i < num; ++i)
    {
        float x[3];
        x[0] = coords[3*i+0];
        x[1] = coords[3*i+1];
        x[2] = coords[3*i+2];

        int idx = floor((x[0]-origin[0])/basis[0][0]);
        int idy = floor((x[1]-origin[1])/basis[1][1]);
        int idz = floor((x[2]-origin[2])/basis[2][2]);

        if(idz >= 0 && (size_t)idz < nz && 
           idy >= 0 && (size_t)idy < ny &&
           idx >= 0 && (size_t)idx < nx )
        {
            int id = idz + nz * (idy + ny*idx);
            #pragma omp atomic
            data[id] += 1;   
        } 
    }    
}

static void make_pmf(void* new_grid, void* grid, int numframes, float radius)
{
    size_t nx = get_nx(grid);
    size_t ny = get_ny(grid);
    size_t nz = get_nz(grid);
    size_t num = nx*ny*nz;

    double* origin = get_grid_origin(grid);
    double** basis = get_grid_basis(grid);

    double* data = get_grid_data(grid);
    double* new_data = get_grid_data(new_grid);

    #pragma omp parallel for
    for(size_t i = 0; i < num; ++i)
        data[i] /= numframes;
    #pragma omp parallel for
    for(size_t i = 0; i < num; ++i)
    {
        float pos[3];
        int id[3];
        id[0] = i / nz / ny;
        id[1] = i / nz % ny;
        id[2] = i % nz;
        pos[0] = (float)(origin[0]+basis[0][0]*(id[0]+0.5));
        pos[1] = (float)(origin[1]+basis[1][1]*(id[1]+0.5));
        pos[2] = (float)(origin[2]+basis[2][2]*(id[2]+0.5));
 
        if(data[i] > 0 && is_in_grid_size(grid, pos, radius))
        //if(data[i] > 1e-8)
            new_data[i] = 1;
        else
            new_data[i] = 0; 
    }
}

static void _extract_arguments(double* lx, double* ly, double* lz, 
                               int* nx, int* ny, int* nz, double* origin, 
                               char* traj_name, int* start, int* end, int* skip, float* radius,
                               char** argv, int argc)
{
    for (int i = 0; i < argc; ++i)
    {
        if (!strcmp (argv[i], "-traj"))
        {
            strcpy (traj_name, argv[i+1]);
            *start = atoi (argv[i+2]);
            *end = atoi (argv[i+3]);
            *skip = atoi (argv[i+4]);
            i += 4;
        }
        else if (!strcmp (argv[i], "-l"))
        {
            *lx = (double)atof(argv[i+1]);
            *ly = (double)atof(argv[i+2]);
            *lz = (double)atof(argv[i+3]);
            i += 3;
        }
        else if (!strcmp (argv[i], "-n"))
        {
            *nx = (size_t)atoi(argv[i+1]);
            *ny = (size_t)atoi(argv[i+2]);
            *nz = (size_t)atoi(argv[i+3]);
            i += 3;
        }
        else if (!strcmp (argv[i], "-origin"))
        {
            origin[0] = (double)atof(argv[i+1]);
            origin[1] = (double)atof(argv[i+2]);
            origin[2] = (double)atof(argv[i+3]);

            i += 3;
        }
        else if (!strcmp (argv[i], "-radius"))
        {
            *radius = atof(argv[i+1]);

            i += 1;
        }
    }
}
#endif

static void intersection(void* grid1, void* grid2)
{
    size_t nx = get_nx(grid1);
    size_t ny = get_ny(grid1);
    size_t nz = get_nz(grid1);

    size_t num = nx*ny*nz;

    double* data1 = get_grid_data(grid1);
    double* data2 = get_grid_data(grid2);

    #pragma omp parallel for
    for(size_t i = 0; i < num; ++i)
    {
        double val1 = data1[i];
        double val2 = data2[i];
        int b1;
        int b2;
        if(val1 < 0.5)
            b1 = 0;
        else
            b1 = 1;
        if(val2 < 0.5)
            b2 = 0;
        else
            b2 = 1;
        data1[i] = (b1 & b2);  
    }
}
 
int main(int argc, char** argv)
{
    void* grid1, *grid2;
    grid1 = gridformat_reader(argv[1]);
    grid2 = gridformat_reader(argv[2]);

    intersection(grid1, grid2);
    gridformat_writer(argv[3], grid1);

    grid_destroy(grid1);
    grid_destroy(grid2);

    return 0;
}
