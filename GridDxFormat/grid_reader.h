#ifndef GRID_READER_H_
#define GRID_READER_H_
#include <stddef.h>
//void remove_new_line(char* str);
//void remove_white_space(char* str);
void* gridformat_reader(const char* name);
void grid_destroy(void* src);
int gridformat_writer(char* name, void* g);
int gridformat_fwriter(char* name, void* g);
double* get_grid_origin(void* src);
double** get_grid_basis(void* src);
double* get_grid_data(void* src);
size_t get_nx(void* src);
size_t get_ny(void* src);
size_t get_nz(void* src);
size_t get_size(void* src);
void* copy_grid_format(void*);
typedef double (*compute_type)(size_t,size_t,size_t,double*,double**, void*);
void set_grid_data(void* src, compute_type compute, void* kernel);
void* resample_grid_data(void* src);
void sum_grids(void* dest, int num, void** src);
double total_elements(void* src);
void sub_grids(void* dest, void* s1, void* s2);
void pad_grid(void** g, int nx, int ny, int nz);
void slice_grid(void** g, int nx, int ny, int nz);
void* create_new_grid(size_t nx, size_t ny, size_t nz, double* origin, double** basis);
void multiply_constant(void* dest, double a);
void* copy_grid(void* ptr);
int is_in_grid (void* _grid, float* pos);
int is_in_grid_size (void* _self, float* pos, float radius);
int is_in_grid_size_periodic (void* _self, float* pos, float radius);
#endif
