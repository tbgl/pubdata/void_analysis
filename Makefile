TUTORIAL:=tutorial.pdf
all:${TUTORIAL}

${TUTORIAL}: tutorial.tex
	pdflatex tutorial.tex

clean:
	rm -r -f *.pdf *.aux *.log *.gz


