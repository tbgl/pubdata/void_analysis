#ifndef CHANNEL_H_
#define CHANNEL_H_
void find_connectivity (void* _pmf, void* mol, float* coords, double* cell, float rc, 
float r_max, float r_constraint, float cut_off, void* lipid);
void* create_bond_graph (double* cell, double* origin, int* N);
void clean_bond_graph(void*);
#endif
