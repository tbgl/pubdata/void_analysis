#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <string.h>
#include <omp.h>
#include "channel.h"
#include "grid_reader.h"
typedef struct BondGraph
{
    double origin[3];
    int N[3];
    double delta[3];
    int64_t num;
    int64_t* id_list;
    int64_t* size;
    int64_t num_heads;
    int64_t max_cluster_id;
    int64_t max_cluster_size;
} BondGraph;

typedef struct Molecules
{
    int num;
    int num_types;
    int* type;
    float* mass;
    float* radius;
} Molecules;

void clean_bond_graph (void* _graph)
{
    BondGraph* graph = (BondGraph*)_graph;
    free(graph->id_list);
    free(graph->size);
}

void _reset_bond_graph(BondGraph* graph)
{
    graph->num_heads = 0;
    omp_set_num_threads(8);
    #pragma omp parallel for
    for(int64_t i = 0; i < graph->num; ++i)
    {
        graph->id_list[i] = -1;
        graph->size[i] = 0;
    }
    graph->id_list[graph->num+0] = graph->num+0;
    graph->id_list[graph->num+1] = graph->num+1;
}

void* create_bond_graph (double* cell, double* origin, int* N)
{
    BondGraph* graph = malloc (sizeof (BondGraph));
    graph->origin[0] = origin[0];
    graph->origin[1] = origin[1];
    graph->origin[2] = origin[2];
    graph->N[0] = N[0];
    graph->N[1] = N[1];
    graph->N[2] = N[2];
    graph->num = N[0]*N[1]*N[2];
    graph->delta[0] = cell[0]/N[0];
    graph->delta[1] = cell[2]/N[1];
    graph->delta[2] = cell[5]/N[2];
    //graph->is_vacant = malloc(sizeof(int32_t)*((graph->size+31)/32));
    //memset(graph->is_vacant, 0, sizeof(int32_t)*((graph->size+31)/32));

    graph->id_list = malloc(sizeof(int64_t)*(graph->num+2));
    graph->size = malloc(sizeof(int64_t)*graph->num);
    //_reset_bond_graph(graph)
 
    return graph;
}

static int64_t _find(BondGraph* graph, int64_t i)
{
    for(; i != graph->id_list[i]; i = graph->id_list[i])
    {
        graph->id_list[i] = graph->id_list[graph->id_list[i]];
    }
    return i;
}

static void _add_connection(BondGraph* graph, int64_t i, int64_t j)
{
    if ((i = _find(graph, i)) != (j = _find(graph, j)))
    {
        if(graph->size[i] < graph->size[j])
        {
            graph->id_list[i] = j;
            graph->size[j] += graph->size[i];
            graph->size[i] = 0;
        }
        else
        {
            graph->id_list[j] = i;
            graph->size[i] += graph->size[j];
            graph->size[j] = 0;
        }
        graph->num_heads--;
    }
}

static int _is_connected(BondGraph* graph, int64_t i, int64_t j)
{
    return _find(graph, i) == _find(graph, j);
}

static void _add_connections(BondGraph* graph)
{
    //omp_set_num_threads(8);
    //#pragma omp parallel for
    for(int64_t i = 0; i < graph->num; ++i)
    {
        if(graph->id_list[i] >= 0)
        {
            int64_t ix = i / graph->N[2] / graph->N[1];
            int64_t iy = i / graph->N[2] % graph->N[1];
            int64_t iz = i % graph->N[2];
            for(int dx = -1; dx <= 1; ++dx) 
            {
                for(int dy = -1; dy <= 1; ++dy)
                {
                    for(int dz = -1; dz <= 1; ++dz)
                    {
                        int64_t jx = ix+dx;
                        int64_t jy = iy+dy;
                        int64_t jz = iz+dz;
                        if(jx >= 0 && jx < graph->N[0] &&  
                           jy >= 0 && jy < graph->N[1] &&
                           jz >= 0 && jz < graph->N[2]
                          )
                        {
                            int64_t j = jz + graph->N[2]*(jy + graph->N[1]*jx);
                            if(graph->id_list[j] >= 0 && j > i)
                                //#pragma omp critical
                                _add_connection(graph, i, j);
                            //if(graph->id_list[j] >= 0 && iz == jz && (iz == 0 || iz == graph->N[2]-1) && j > i)
                                //_add_connection(graph, i, j);
                        }
                    }
                }
            }
        }
    }
    for(int64_t x = 0; x < graph->N[0]; ++x)
    {
        for(int64_t y = 0; y < graph->N[1]; ++y)
        {
            int64_t idx = graph->N[2]*(y+graph->N[1]*x); 
            if(graph->id_list[idx] >= 0)
                _add_connection(graph, graph->num, idx);  
        }
    }
    for(int64_t x = 0; x < graph->N[0]; ++x)
    {
        for(int64_t y = 0; y < graph->N[1]; ++y)
        {
            int64_t idx = graph->N[2]-1+graph->N[2]*(y+graph->N[1]*x);
            if(graph->id_list[idx] >= 0)       
                _add_connection(graph, graph->num+1, idx);
        }
    }    
}

static int _has_full_path(BondGraph* graph)
{
    return _is_connected(graph, graph->num, graph->num+1);
}

static int64_t _max_cluster_size(BondGraph* g)
{
    int64_t max = 0;
    int64_t id = -1; 
    for(int64_t i = 0; i < g->num; ++i)
    {
        if(g->size[i] > max)
        {
            id = i;
            max = g->size[i];
        }
    }
    g->max_cluster_size = max;
    g->max_cluster_id = id;
    return max;
}

static double _mean_cluster_size(BondGraph* g)
{
    double s2ns = 0.;
    double sns = 0.;
    omp_set_num_threads(16);
    #pragma omp parallel for reduction(+:s2ns, sns)
    for(int64_t i = 0; i < g->num; ++i)
    {
        if(i != g->max_cluster_id)
        {
            s2ns += (double)(g->size[i]*g->size[i])/(double)g->num;
            sns  += (double)g->size[i]/(double)g->num;  
        } 
    }
    return s2ns/sns;
}

static double _char_cluster_size(BondGraph* g)
{
    double char_size = 0.;
    for(int64_t i = 0; i < g->num; ++i)
    {
        if(i != g->max_cluster_id && char_size < g->size[i])
            char_size = g->size[i];
    }
    return char_size;
}

static void _create_link_cell (int* head, int* link_cell, float* coords, int num_atoms, double* origin, int* ncell, float rc)
{
  /* Reset the headers, head */
  int lxyz = ncell[0] * ncell[1] * ncell[2];  
  for (int c = 0; c < lxyz; c++) 
      head[c] = -1;  
  /* Scan atoms to construct headers, head, & linked lists, lscl */  
  for (int i = 0; i < num_atoms; i++) 
    {    
      /* Vector cell index to which this atom belongs */
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];

      pos[0] -= origin[0];
      pos[1] -= origin[1];
      pos[2] -= origin[2]; 
      int mc[3];
      mc[0] = floor(pos[0] / rc);
      mc[1] = floor(pos[1] / rc);
      mc[2] = floor(pos[2] / rc);   

      //mc[0] = (mc[0]+ncell[0]) % ncell[0];
      //mc[1] = (mc[1]+ncell[1]) % ncell[1];
      //mc[2] = (mc[2]+ncell[2]) % ncell[2]; 
      if (mc[0] >= 0 && mc[0] < ncell[0] && mc[1] >= 0 && mc[1] < ncell[1] && mc[2] >= 0 && mc[2] < ncell[2])
        {
          /* Translate the vector cell index, mc, to a scalar cell index */    
          int c = mc[2] + ncell[2] * (mc[1] + ncell[1] * mc[0]);
       
          /* Link to the previous occupant (or EMPTY if you're the 1st) */    
          link_cell[i] = head[c];    
          /* The last one goes to the header */    
          head[c] = i;  
        }
    }
}

static void _find_home_cell (int* cell_id, float* pos, double* origin, float rc)
{
  float dr[3];
  dr[0] = pos[0]-origin[0];
  dr[1] = pos[1]-origin[1];
  dr[2] = pos[2]-origin[2];

  cell_id[0] = floor(dr[0]/rc);
  cell_id[1] = floor(dr[1]/rc);
  cell_id[2] = floor(dr[2]/rc);
}

//void insert_sphere (void* _pmf, void* _mol, float* coords, double* cell, float rc, 
//float r_max, float r_constraint, float cut_off)
void find_connectivity 
(
    void* _graph, 
    void* _mol, 
    float* coords, 
    double* cell, 
    float rc, 
    float r_max, 
    float r_constraint, 
    float cut_off, 
    void* lipid_grid
)
{
    Molecules* mol = (Molecules*)_mol;
    float* radius = mol->radius;
    int* type = mol->type;
    int num_atoms = mol->num;

    //set origin
    double origin[3];
    origin[0] = -0.5*cell[0];
    origin[1] = -0.5*cell[2];
    origin[2] = -0.5*cell[5];

    int ncell[3];
    ncell[0] = ceil (cell[0] / rc);
    ncell[1] = ceil (cell[2] / rc);
    ncell[2] = ceil (cell[5] / rc);

    int num_cells = ncell[0]*ncell[1]*ncell[2];
    int* head = (int*) malloc (sizeof (int)*num_cells);
    int* link_cell = (int*) malloc (sizeof (int)*num_atoms);
    //int neighs = (int) ceil (r_max/rc);
    int neighs = (int) ceil (cut_off/rc);
    _create_link_cell (head, link_cell, coords, num_atoms, origin, ncell, rc);  
 
    //scan all of the voxels
    BondGraph* g = (BondGraph*)_graph;
    //reset bond graph
    _reset_bond_graph(g);
    int64_t total_vacants = 0;
    int64_t nv = g->num;
    omp_set_num_threads(16);
    #pragma omp parallel for
    for (int64_t i = 0; i < nv; ++i)
    {
        float pos[3];
        int64_t id[3];
        id[0] = i / g->N[2] / g->N[1];
        id[1] = i / g->N[2] % g->N[1];
        id[2] = i % g->N[2];
        pos[0] = g->origin[0]+g->delta[0]*((float)id[0]+0.5f);
        pos[1] = g->origin[1]+g->delta[1]*((float)id[1]+0.5f);
        pos[2] = g->origin[2]+g->delta[2]*((float)id[2]+0.5f);
        if (!is_in_grid(lipid_grid, pos))
            continue;
        //printf("%lld %lld %lld %f %f %f\n", (long long)id[0], (long long)id[1], (long long)id[2], g->origin[0], g->origin[1], g->origin[2]);
        int home_cell[3];
        _find_home_cell (home_cell, pos, origin, rc);
        float probe_radius = (float)INT_MAX;
        //int has_sphere = 0;
        float rho2 = pos[0]*pos[0]+pos[1]*pos[1];
        rho2 = sqrt (rho2);
        if (rho2 > r_constraint)
            //probe_radius = -(float)INT_MAX;
            continue;
        else
        {
            for (int dx = -neighs; dx <= neighs; ++dx)
            { 
                for (int dy = -neighs; dy <= neighs; ++dy)
                {
                    for (int dz = -neighs; dz <= neighs; ++dz)
                    {
                        int cell_id[3];
                        cell_id[0] = home_cell[0]+dx;
                        cell_id[1] = home_cell[1]+dy;
                        cell_id[2] = home_cell[2]+dz;
                        if(cell_id[0] < 0 || cell_id[0] >= ncell[0] || cell_id[1] < 0 || 
                            cell_id[1] >= ncell[1] || cell_id[2] < 0 || cell_id[2] >= ncell[2])
                            continue;
                        int c = cell_id[2] + ncell[2] * (cell_id[1] +ncell[1] *cell_id[0]);
                        int atom_id = head[c];
                        while (atom_id != -1)
                        {
                            float r[3];
                            r[0] = coords[3*atom_id+0];
                            r[1] = coords[3*atom_id+1];
                            r[2] = coords[3*atom_id+2];

                            r[0] = (pos[0]-r[0]);
                            r[1] = (pos[1]-r[1]);
                            r[2] = (pos[2]-r[2]);

                            float dist2 = r[0]*r[0]+r[1]*r[1]+r[2]*r[2];
                            dist2 = sqrt (dist2);
                            float diff = dist2 - radius[type[atom_id]]-r_max;
                            if (diff < probe_radius)
                                probe_radius = diff;
                            if (probe_radius <= 0)
                                break;
                            atom_id = link_cell[atom_id];
                        }
                        if (probe_radius <= 0)
                            break; 
                    }
                    if(probe_radius <= 0)
                        break;
                }
                if (probe_radius <= 0)
                    break;
            }
        }
        if (probe_radius > 0.)// && is_in_grid(lipid_grid, pos))
        {
            g->id_list[i] = i; 
            g->size[i] = 1;
            #pragma omp critical
            {
                g->num_heads++;
                //total_vacants++;
            }
        }
    }
    //do average here
    //average_1d_pmf_steric (_pmf);
    free (head);
    free (link_cell);

    _add_connections(g);
    int has_path = _has_full_path(g);
    //double char_cluster_size = _char_cluster_size(g);

    printf("%d\n", has_path);
    fflush(stdout);
}
