#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void trim_both_sides(char* line)
{
    int len = strlen(line);
    if(len > 0)
    {
        for(int i = len-1; i >= 0; --i)
        {
            if(!isspace(line[i]))
            {
                line[i+1] = '\0';
                break;
            }
        }
    }
    int pos = 0;
    len = strlen(line);
    if(len > 0)
    {
        for(pos = 0; pos < len; ++pos)
        {
            if(!isspace(line[pos]))
                break;
        }
        memmove((void*)line, (void*)(line+pos), len-pos+1);
    } 
}

void extract_arguments(char*** argv, int* argc, char* command)
{
    char* buff = (char*)malloc(sizeof(char)*(strlen(command)+1));
    strcpy(buff, command);
    int cap = 16;
    if(*argv == NULL && *argc == 0)
    {
        *argv = (char**)malloc(sizeof(char*)*cap);
        memset(*argv, 0, sizeof(char*)*cap);
    }
    else if(*argv != NULL && *argc != 0)
        cap = *argc;
    else
    {
        fprintf(stderr, "error initialization %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    *argc = 0;
    char* save_ptr = NULL, *ptr = NULL;

    ptr = strtok_r(buff, " \t", &save_ptr);
    if(ptr == NULL)
    {
        fprintf(stderr, "error string %s, %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    (*argv)[*argc] = (char*)realloc((*argv)[*argc], sizeof(char)*(strlen(ptr)+1));
    strcpy((*argv)[*argc], ptr);
    *argc += 1;
    while((ptr = strtok_r(NULL, " \t", &save_ptr)) != NULL)
    {
        if(*argc >= cap)
        {
            //cap += 8;
            *argv = (char**)realloc(*argv, sizeof(char*)*(cap+16));
            memset((*argv)+cap, 0, sizeof(char*)*16);
            cap += 16;
        }
        (*argv)[*argc] = (char*)realloc((*argv)[*argc], sizeof(char)*(strlen(ptr)+1));
        strcpy((*argv)[*argc], ptr);
        *argc +=1;
    }
    if(*argc < cap)
    {
        for(int i = *argc; i < cap; ++i)
            free((*argv)[i]);
    }
    *argv = (char**)realloc(*argv, sizeof(char*)*(*argc));
    //free(save_ptr);
    free(buff);
}

int extract_options (char** argv, int argc, char* option, int num_options)
{
  for (int i = 0; i < argc; ++i)
    {
      if (!strcmp (argv[i], option))
        {
          if (i+num_options >= argc)
            {
              fprintf (stderr, "error arguments %s %d\n", __FILE__, __LINE__);
              exit (-1);   
            }
          return i; 
        }
    }
  return -1;
}

int extract_num_options (char** argv, int argc, char* option)
{
  int count = 0;
  for (int i = 0; i < argc; ++i)
    {
      if (!strcmp (argv[i], option))
        {
          for (int j = i+1; j < argc; ++j)
            {
              if (argv[j][0] == '-')
                {
                  if (strlen (argv[j]) > 1 && isdigit (argv[j][1]))
                    count++;
                  else if (!isdigit (argv[j][1]))
                    return count;
                  else if (strlen (argv[j]) <= 1)
                    return -1;
                }
              else
                count++;
            } 
        }
    }
  return count;
}

