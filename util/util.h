#ifndef UTIL_H_
#define UTIL_H_
void trim_both_sides(char* line);
void extract_arguments(char*** argv, int* argc, char* command);
//will write a function call extract options
int extract_options (char** argv, int argc, char* option, int num_options);
int extract_num_options (char** argv, int argc, char* option);
#endif
