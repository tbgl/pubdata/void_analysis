#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <string.h>
#include "channel.h"

typedef struct Sphere
{
  float pos[3];
  float radius;  
} Sphere;

typedef struct PMF
{
  double origin[3];
  int N[3];
  double delta[3];
  double* statistics;
  double* pmf;
} PMF;

typedef struct Molecules
{
  int num;
  int num_types;
  int* type;
  float* mass;
  float* radius;
  //double cell[6];
} Molecules;

void clean_pmf (void* _pmf)
{
  PMF* pmf = (PMF*)_pmf;
  free (pmf->pmf);
  free (pmf->statistics);
}

void average_1d_pmf_steric (void* _pmf)
{
  PMF* pmf = (PMF*)_pmf;
  for (int k = 0; k < pmf->N[2]; ++k)
    {
      for (int i = 0; i < pmf->N[0]; ++i)
        {
          for (int j = 0; j < pmf->N[1]; ++j)
            {
              int id = k + pmf->N[2]*(j+pmf->N[1]*i); 
              pmf->pmf[k] += pmf->statistics[id];//(pmf->N[0]*pmf->N[1]);
            }
        } 
    }
}

void* MallocPMF (double* cell, double* origin, int* N)
{
  PMF* pmf = malloc (sizeof (PMF));
  pmf->origin[0] = origin[0];
  pmf->origin[1] = origin[1];
  pmf->origin[2] = origin[2];
  pmf->N[0] = N[0];
  pmf->N[1] = N[1];
  pmf->N[2] = N[2];
  pmf->statistics = malloc (sizeof (double)*N[0]*N[1]*N[2]);
  memset (pmf->statistics, 0, sizeof (double)*N[0]*N[1]*N[2]);
  pmf->delta[0] = cell[0]/N[0];
  pmf->delta[1] = cell[2]/N[1];
  pmf->delta[2] = cell[5]/N[2];
  pmf->pmf = malloc (sizeof (double)*N[2]);
  memset (pmf->pmf, 0, sizeof (double)*N[2]);

  return pmf;
}

//this will average the right and left
void boltzmann_inversion_pmf_1d (void* _pmf)
{
  PMF* pmf = (PMF*)_pmf;
  double* tmp = malloc (sizeof (double)*pmf->N[2]);
  for(int i = 0; i < pmf->N[2]; ++i)
    {
      if (pmf->pmf[i] <= 0.)
        pmf->pmf[i] = 1;
    }
  for(int i = 0; i < pmf->N[2]; ++i)
    {
      tmp[i] = pmf->pmf[pmf->N[2]-1-i];
    }
  for(int i = 0; i < pmf->N[2]; ++i)
    {
      pmf->pmf[i] = 0.5*(pmf->pmf[i]+tmp[i]);
    }
  double normalize = pmf->pmf[0];
  for (int i = 1; i < pmf->N[2]; ++i)
    {
      double val = pmf->pmf[i];
      if (val > normalize)
          normalize = val;
      //pmf->pmf[i] /= normalize;
    }

  for (int i = 0; i < pmf->N[2]; ++i)
    {
      pmf->pmf[i] /= normalize;
    }
  for (int i = 0; i < pmf->N[2]; ++i)
    {
      float z = pmf->delta[2]*(i+0.5);
      z = pmf->origin[2]+z;
      printf ("%f %f\n", z, -log(pmf->pmf[i]));
    }
  free (tmp);
}

//this will average the right and left
void radius_1d (void* _pmf, int num_frames)
{
  PMF* pmf = (PMF*)_pmf;
  double* tmp = malloc (sizeof (double)*pmf->N[2]);
  for(int i = 0; i < pmf->N[2]; ++i)
      tmp[i] = pmf->pmf[pmf->N[2]-1-i];
  for(int i = 0; i < pmf->N[2]; ++i)
      pmf->pmf[i] = 0.5*(pmf->pmf[i]+tmp[i]);

  for (int i = 0; i < pmf->N[2]; ++i)
  {
      double da = pmf->delta[0]*pmf->delta[1];
      double r = da*pmf->pmf[i]/num_frames;
      r = sqrt(r/M_PI);
      float z = pmf->delta[2]*(i+0.5);
      z = pmf->origin[2]+z;
      printf ("%f %lf\n", z, r);
  }
  free (tmp);
}

static void _create_link_cell (int* head, int* link_cell, float* coords, int num_atoms, double* origin, int* ncell, float rc)
{
  /* Reset the headers, head */
  int lxyz = ncell[0] * ncell[1] * ncell[2];  
  for (int c = 0; c < lxyz; c++) 
      head[c] = -1;  
  /* Scan atoms to construct headers, head, & linked lists, lscl */  
  for (int i = 0; i < num_atoms; i++) 
    {    
      /* Vector cell index to which this atom belongs */
      float pos[3];
      pos[0] = coords[3*i+0];
      pos[1] = coords[3*i+1];
      pos[2] = coords[3*i+2];

      pos[0] -= origin[0];
      pos[1] -= origin[1];
      pos[2] -= origin[2]; 
      int mc[3];
      mc[0] = floor(pos[0] / rc);
      mc[1] = floor(pos[1] / rc);
      mc[2] = floor(pos[2] / rc);   

      //mc[0] = (mc[0]+ncell[0]) % ncell[0];
      //mc[1] = (mc[1]+ncell[1]) % ncell[1];
      //mc[2] = (mc[2]+ncell[2]) % ncell[2]; 
      if (mc[0] >= 0 && mc[0] < ncell[0] && mc[1] >= 0 && mc[1] < ncell[1] && mc[2] >= 0 && mc[2] < ncell[2])
        {
          /* Translate the vector cell index, mc, to a scalar cell index */    
          int c = mc[2] + ncell[2] * (mc[1] + ncell[1] * mc[0]);
       
          /* Link to the previous occupant (or EMPTY if you're the 1st) */    
          link_cell[i] = head[c];    
          /* The last one goes to the header */    
          head[c] = i;  
        }
    }
}

static void _find_home_cell (int* cell_id, float* pos, double* origin, float rc)
{
  float dr[3];
  dr[0] = pos[0]-origin[0];
  dr[1] = pos[1]-origin[1];
  dr[2] = pos[2]-origin[2];

  cell_id[0] = floor(dr[0]/rc);
  cell_id[1] = floor(dr[1]/rc);
  cell_id[2] = floor(dr[2]/rc);
}

//void insert_sphere (void* _pmf, void* _mol, float* coords, double* cell, float rc, 
//float r_max, float r_constraint, float cut_off)
void insert_sphere (void* _pmf, void* _mol, float* coords, double* cell, float rc, 
float r_max, float r_constraint, float cut_off, void* lipid_grid)
{
  Molecules* mol = (Molecules*)_mol;
  float* radius = mol->radius;
  int* type = mol->type;
  int num_atoms = mol->num;

  //set origin
  double origin[3];
  origin[0] = -0.5*cell[0];
  origin[1] = -0.5*cell[2];
  origin[2] = -0.5*cell[5];

  //printf ("%lf %lf %lf\n", origin[0], origin[1], origin[2]);
  //create link cell list
  int ncell[3];
  ncell[0] = ceil (cell[0] / rc);
  ncell[1] = ceil (cell[2] / rc);
  ncell[2] = ceil (cell[5] / rc);

  int num_cells = ncell[0]*ncell[1]*ncell[2];
  int* head = (int*) malloc (sizeof (int)*num_cells);
  int* link_cell = (int*) malloc (sizeof (int)*num_atoms);
  //int neighs = (int) ceil (r_max/rc);
  int neighs = (int) ceil (cut_off/rc);
  _create_link_cell (head, link_cell, coords, num_atoms, origin, ncell, rc);  
 
  int capacity = 32;
  int count = 0;
  Sphere** sphere = (Sphere**) malloc (sizeof (Sphere*)*capacity);
  //scan all of the voxels
  PMF* pmf = (PMF*)_pmf;
  int total_pmf_cells = pmf->N[0]*pmf->N[1]*pmf->N[2];
  //memset (pmf->statistics, 0, sizeof (double)*pmf->N[0]*pmf->N[1]*pmf->N[2]);
  for (int i = 0; i < total_pmf_cells; ++i)
    {
      float pos[3];
      int id[3];
      id[0] = i / pmf->N[2] / pmf->N[1];
      id[1] = i / pmf->N[2] % pmf->N[1];
      id[2] = i % pmf->N[2];
      pos[0] = pmf->origin[0]+pmf->delta[0]*(id[0]+0.5);
      pos[1] = pmf->origin[1]+pmf->delta[1]*(id[1]+0.5);
      pos[2] = pmf->origin[2]+pmf->delta[2]*(id[2]+0.5);
      //pmf->get_position (pos, _pmf, id);
      //determine home patch here
      int home_cell[3];
      _find_home_cell (home_cell, pos, origin, rc);
      float probe_radius = (float)INT_MAX;
      //int has_sphere = 0;
      float rho2 = pos[0]*pos[0]+pos[1]*pos[1];
      rho2 = sqrt (rho2);
      if (rho2 > r_constraint)
          probe_radius = -(float)INT_MAX;
      else
        {
          for (int dx = -neighs; dx <= neighs; ++dx)
            { 
              for (int dy = -neighs; dy <= neighs; ++dy)
                {
                  for (int dz = -neighs; dz <= neighs; ++dz)
                    {
                      int cell_id[3];
                      cell_id[0] = home_cell[0]+dx;
                      cell_id[1] = home_cell[1]+dy;
                      cell_id[2] = home_cell[2]+dz;
                      if (cell_id[0] < 0 || cell_id[0] >= ncell[0] || cell_id[1] < 0 || 
                          cell_id[1] >= ncell[1] || cell_id[2] < 0 || cell_id[2] >= ncell[2])
                        continue;
                      int c = cell_id[2] + ncell[2] * (cell_id[1] +ncell[1] *cell_id[0]);
                      int atom_id = head[c];
                      while (atom_id != -1)
                        {
                          float r[3];
                          r[0] = coords[3*atom_id+0];
                          r[1] = coords[3*atom_id+1];
                          r[2] = coords[3*atom_id+2];

                          r[0] = (pos[0]-r[0]);
                          r[1] = (pos[1]-r[1]);
                          r[2] = (pos[2]-r[2]);

                          float dist2 = r[0]*r[0]+r[1]*r[1]+r[2]*r[2];
                          dist2 = sqrt (dist2);
                          float diff = dist2 - radius[type[atom_id]]-r_max;
                          if (diff < probe_radius)
                            {  
                              probe_radius = diff;
                            }
                          if (probe_radius <= 0)
                            break;
                          atom_id = link_cell[atom_id];
                        }
                      if (probe_radius <= 0)
                        break; 
                    }
                  if(probe_radius <= 0)
                    break;
                }
              if (probe_radius <= 0)
                break;
            }
        }
      if (probe_radius > 0. && is_in_grid(lipid_grid, pos))
        pmf->statistics[i]++;
    }
  //do average here
  //average_1d_pmf_steric (_pmf);
  free (head);
  free (link_cell);
  for (int i = 0; i < count; ++i)
    {
      free (sphere[i]);
    }
  free (sphere);
}
