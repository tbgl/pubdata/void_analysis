import numpy as np
import sys

D0=float(sys.argv[2])
zs, radii = np.loadtxt(sys.argv[1], unpack=True)
dz = zs[1]-zs[0]
D=np.zeros(len(zs))
for i, r in enumerate(radii):
    dwdz = 0
    if i == 0:
        dwdz = (radii[1]-radii[0])/dz
    elif i == len(zs)-1:
        dwdz = (radii[-1]-radii[-2])/dz
    else:
        dwdz =  (radii[i+1]-radii[i-1])/(2*dz)
    D[i]=D0/((1+dwdz*dwdz)**0.5)
tmp = D[::-1]
D = 0.5*(D+tmp)
f = open("diff.txt", "wt")
for i in range(len(zs)):
    f.write(str(zs[i])+" "+str(D[i])+"\n") 
f.close()
