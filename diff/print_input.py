import sys

n = int(sys.argv[1])

f=open(sys.argv[2],"w")

f.write("Beads\n1\n0 1.0 3.0\n")

f.write("Molecules\n")
f.write(sys.argv[1]+"\n")
for i in range(n):
    f.write(str(i)+" 0\n")
f.close()

