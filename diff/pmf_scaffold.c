#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "channel.h"
#include "volmap.h"
#include "util.h"
#include "grid_reader.h"

static void _extract_arguments(char* traj_name, char* mol_name, float* rc, float* r_max, 
float* r_constraint, float* cut_off, double* cell, int* N, char* lipid_name, char** argv, int argc)
{
    for (int i = 0; i < argc; ++i)
      {
        if (!strcmp (argv[i], "-traj"))
          {
            strcpy (traj_name, argv[i+1]);
            i += 1;         
          }
        else if (!strcmp (argv[i], "-mol"))
          {
            strcpy (mol_name, argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-rc"))
          {
            *rc = atoi (argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-probe"))
          {
            *r_max = atof (argv[i+1]);
            i += 1;   
          }
        else if (!strcmp (argv[i], "-cut_off"))
          {
            *cut_off = atof (argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-constraint"))
          {
            *r_constraint = atof (argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-lipid_name"))
          {
            strcpy (lipid_name, argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-cell"))
          {
            cell[0] = atof (argv[i+1]);
            cell[2] = atof (argv[i+2]);
            cell[5] = atof (argv[i+3]);
            N[0] = atoi (argv[i+4]);
            N[1] = atoi (argv[i+5]);
            N[2] = atoi (argv[i+6]);
            i += 6;
          }  
      }
}

static float* read_xyz_coords (char* filename)
{
  FILE* fptr = fopen(filename, "rt");
  if (fptr == NULL)
    exit(-1);
  char* line = NULL;
  size_t nbytes = 0;
  float* coords = NULL;
  char** argv = NULL;
  int argc = 0;
  int count = 0;
  int N = 0;
  while(getline (&line,&nbytes, fptr) >= 0) 
    {
      trim_both_sides (line);
      if (strlen(line) == 0)
        continue;
      if (line[0] == '#')
        {
          extract_arguments (&argv, &argc, line);
          N = atoi(argv[argc-1]);
          coords = malloc(sizeof(float)*N*3);
          continue;
        }
      extract_arguments (&argv, &argc, line);
      coords[3*count+0] = atof(argv[0]);
      coords[3*count+1] = atof(argv[1]);
      coords[3*count+2] = atof(argv[2]);
      count++;
      if (count > N)
        exit(-1);
    }
  free(line);
  for (int i = 0; i < argc; ++i)
    free (argv[i]);
  free(argv);
  fclose(fptr);
  return coords;
}

int main (int argc, char** argv)
{
  char traj_name[1024];
  traj_name[0] = '\0';
  char mol_name[1024];
  mol_name[0] = '\0';

  char lipid_name[1024];
  lipid_name[0] = '\0';

  float rc, r_max, r_constraint;
  float cut_off = 90.0f;
  double cell[6] = {0.,0.,0.,0.,0.,0.};
  int N[3];
  _extract_arguments(traj_name, mol_name, &rc, &r_max,&r_constraint, &cut_off, cell, N, lipid_name, argv, argc);
  void* molecules = create_molecules (mol_name);
  double origin[3];
  origin[0] = -cell[0]*0.5;
  origin[1] = -cell[2]*0.5;
  origin[2] = -cell[5]*0.5;
  void* _pmf = MallocPMF (cell, origin, N); 
  //read coordinates text
  float* coords = read_xyz_coords (traj_name);
  //double* cell  = get_unitcell (traj_handler, t);
  void* lipid_grid =  gridformat_reader(lipid_name);
  insert_sphere (_pmf, molecules, coords, cell, rc, r_max, r_constraint, cut_off, lipid_grid);
  radius_1d (_pmf,1);
  clean_pmf (_pmf);
  clean_molecules (molecules);
  grid_destroy(lipid_grid);
  return 0;
}
