#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "traj_dcd_handler.h"
#include "channel.h"
#include "volmap.h"
#include "util.h"

static void _extract_arguments(char* traj_name, int* start, int* end, int* skip, 
char* mol_name, float* rc, float* r_max, int* is_write, float* cut_off, double* cell, 
int* N, char* pmf_type, char** argv, int argc)
{
    for (int i = 0; i < argc; ++i)
      {
        if (!strcmp (argv[i], "-traj"))
          {
            strcpy (traj_name, argv[i+1]);
            *start = atoi (argv[i+2]);
            *end = atoi (argv[i+3]);
            *skip = atoi (argv[i+4]);
            i += 4;         
          }
        else if (!strcmp (argv[i], "-mol"))
          {
            strcpy (mol_name, argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-rc"))
          {
            ++i;
            *rc = atoi (argv[i]);
          }
        else if (!strcmp (argv[i], "-probe"))
          {
            *r_max = atof (argv[i+1]);
            i += 1;   
          }
        else if (!strcmp (argv[i], "-write"))
          {
            *is_write = atoi (argv[i+1]);
            i += 1;
          } 
        else if (!strcmp (argv[i], "-cut_off"))
          {
            *cut_off = atof (argv[i+1]);
            i += 1;
          }
        else if (!strcmp (argv[i], "-cell"))
          {
            cell[0] = atof (argv[i+1]);
            cell[2] = atof (argv[i+2]);
            cell[5] = atof (argv[i+3]);
            N[0] = atoi (argv[i+4]);
            N[1] = atoi (argv[i+5]);
            N[2] = atoi (argv[i+6]);
            strcpy (pmf_type, argv[i+7]);
            i += 7;
          }  
      }
}

int main (int argc, char** argv)
{
  char traj_name[1024];
  traj_name[0] = '\0';
  int start = 0, end = 1, skip = 1;
  char mol_name[1024];
  mol_name[0] = '\0';
  char filename[1024];

  float rc, r_max;
  int is_write = 0;
  float cut_off = 90.0f;
  double cell[6] = {0.,0.,0.,0.,0.,0.};
  int N[3];
  char pmf_type[64];
  _extract_arguments(traj_name, &start, &end, &skip, mol_name, &rc, &r_max, &is_write, &cut_off, cell, N, pmf_type, argv, argc);
  void* traj_handler = create_traj_dcd_handler (traj_name);
  read_dcd (traj_handler, start, end, skip);
  void* molecules = create_molecules (mol_name);
  int numframes = (end-start)/skip;
  double origin[3];
  origin[0] = -cell[0]*0.5;
  origin[1] = -cell[2]*0.5;
  origin[2] = -cell[5]*0.5;
  void* _pmf = MallocPMF(cell, origin, N, pmf_type); 
  //read coordinates text
  for (int t = 0; t < numframes; ++t)
    {
      float* coords = get_coords (traj_handler, t);
      //double* cell  = get_unitcell (traj_handler, t);
      int step = start + skip*t;
      snprintf (filename, sizeof (filename), "%s%.5d%s", "sphere", step, ".txt");
      insert_sphere (_pmf, molecules, coords, cell, rc, r_max, filename, is_write, cut_off);
      memset (filename, 0, sizeof (filename));
    }
  void (*average)(void*);
  if (!strcmp (pmf_type, "1d"))
    average = boltzmann_inversion_pmf_1d;
  else
    average = boltzmann_inversion_pmf_cylinder_2d;
  average (_pmf);
  clean_pmf (_pmf);
  clean_molecules (molecules);
  clean_traj_dcd_handler (traj_handler);

  return 0;
}
