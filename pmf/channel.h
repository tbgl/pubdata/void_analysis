#ifndef CHANNEL_H_
#define CHANNEL_H_
void insert_sphere (void* _pmf, void* mol, float* coords, double* cell, float rc, 
float r_max, float r_constraint, float cut_off, void* lipid);
void* MallocPMF (double* cell, double* origin, int* N);
void boltzmann_inversion_pmf_1d (void* _pmf);
void clean_pmf(void*);
void average_1d_pmf_steric (void* _pmf,int);
#endif
