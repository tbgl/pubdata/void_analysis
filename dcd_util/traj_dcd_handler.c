#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <math.h>

typedef struct dcd_header
{
    char  title[8];
    char  remark[128];
    char  time_stamp[128];
    int   begin_step, end_step;
    int   save_period;
    int   totalframes;
    int   charm_ver;
    float dt;
    int numbeads;
} dcd_header;

typedef struct traj_dcd_handler
{
    char* filename;
    FILE* fptr;
    //int is_header_read;
    dcd_header* header;
    double** unitcell;
    //double** basis;
    //double** inv_basis;
    int capacity;
    int numframes;
    long int bytes_per_frame;
    float** coords;
} traj_dcd_handler;

typedef struct traj_dcd_writer
{
  char* filename;
  FILE* fptr;
  int numbeads;
  float* coords;
  float *xf, *yf, *zf;
  int is_numbeads_set;
  char remark[128];
  int save_period;
  int is_save_period_set;
  float dt; 
  int is_dt_set;
  int is_header_written;
  int numframes;
} traj_dcd_writer;
 
#define DCD_HEADER_OFFSET 276l
#define NFILE_POS 8L
#define NSTEP_POS 20L

static void read_header(FILE* fptr, double** cell)
{
    if(*cell == NULL)
        *cell = (double*)malloc(6*sizeof(double));
    if(*cell == NULL)
    {
        fprintf(stderr, "error in malloc %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    int32_t bytes;
    fread(&bytes, sizeof(int32_t), 1, fptr);
    void* buff = malloc(bytes);
    fread(buff, bytes, 1, fptr);
    if(bytes == 48)
    {
        (*cell)[0] = *((double*)buff+0);
        (*cell)[1] = *((double*)buff+1);
        (*cell)[2] = *((double*)buff+2);
        (*cell)[3] = *((double*)buff+3);
        (*cell)[4] = *((double*)buff+4);
        (*cell)[5] = *((double*)buff+5);
    }
    else if(bytes == 24)
    {
        (*cell)[0] = *((float*)buff+0);
        (*cell)[1] = *((float*)buff+1);
        (*cell)[2] = *((float*)buff+2);
        (*cell)[3] = *((float*)buff+3);
        (*cell)[4] = *((float*)buff+4);
        (*cell)[5] = *((float*)buff+5);
    }
    else
    {
        fprintf(stderr, "error format %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
    free(buff);
} 

static void read_frame(FILE* fptr, float** buff)
{
    int32_t bytes;
    float *tmp;
    fread(&bytes, sizeof(int32_t), 1, fptr);
    if((int)bytes % sizeof(float) != 0)
    {
        fprintf(stderr, "error format %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    int numbeads = (int)bytes / sizeof(float);
    if(*buff == NULL)
        *buff = (float*)malloc(bytes*3);
    tmp  = (float*)malloc(bytes*3);
    if(buff == NULL || tmp == NULL)
    {
        fprintf(stderr, "error in malloc %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    fread(tmp, bytes, 1, fptr);
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
    fread(tmp+numbeads, bytes, 1, fptr);
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
    fread(tmp+numbeads*2, bytes, 1, fptr);
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
    for(int i = 0; i < numbeads; ++i)
    {
        (*buff)[3*i+0] = tmp[i+0*numbeads];
        (*buff)[3*i+1] = tmp[i+1*numbeads];
        (*buff)[3*i+2] = tmp[i+2*numbeads];
    }
    free(tmp);
}

void read_dcd(void* _handler, int start, int end, int skip)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    FILE* fptr = handler->fptr;  
    long int bytes_per_frame = handler->bytes_per_frame;
    fseek(fptr, DCD_HEADER_OFFSET+start*bytes_per_frame, SEEK_SET);
    if(end > handler->header->totalframes)
        end = handler->header->totalframes;
    handler->numframes = 0;
    //fprintf(stderr, "%d %d\n", end, handler->header->totalframes);
    for(int f = start; f < end; f += skip)
    {
        //double* unitcell = read_header(fptr);
        //float*  data = read_frame(fptr);
        //fprintf(stderr, "%d\n", f);
        if(handler->capacity <= handler->numframes)
        {
            //handler->capacity *= 2;
            handler->unitcell = (double**)realloc(handler->unitcell, sizeof(double*)*(handler->capacity+1024));
            memset(handler->unitcell+handler->capacity, 0, sizeof(double*)*1024);
            handler->coords   = (float**) realloc(handler->coords,   sizeof(float*) *(handler->capacity+1024));
            memset(handler->coords+handler->capacity, 0, sizeof(float*)*1024);
            handler->capacity += 1024;
        }
        read_header(fptr, handler->unitcell+handler->numframes);
        read_frame(fptr, handler->coords+handler->numframes);
        //(handler->unitcell)[handler->numframes] = unitcell;
        //(handler->coords)[handler->numframes] = data;
        handler->numframes++;
        if(f+skip < end)
            fseek(fptr, bytes_per_frame*(skip-1), SEEK_CUR);
    }
    //fprintf(stderr, "%d\n", handler->numframes); 
}

static void* read_dcd_header(FILE* fptr)
{
    dcd_header *header = (dcd_header*)malloc(sizeof(dcd_header));
    if(header == NULL)
    {
        fprintf(stderr, "error in malloc %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    memset(header, 0, sizeof(dcd_header));

    int32_t buff;
    fseek(fptr, sizeof(int32_t), SEEK_SET);
    fread(header->title, 4, 1, fptr);
    //fseek(fptr, sizeof(int32_t), SEEK_CUR);
    fread(&buff, sizeof(int32_t), 1, fptr);
    header->totalframes = (int)buff;
    fread(&buff, sizeof(int32_t), 1, fptr);
    header->begin_step = (int)buff;
    fread(&buff, sizeof(int32_t), 1, fptr);
    header->save_period = (int)buff;
    fread(&buff, sizeof(int32_t), 1, fptr);
    header->end_step = (int)buff;
    /*if((header->end_step-header->begin_step)%header->save_period != 0)
    {
        fprintf(stderr, "error format %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }*/
    //printf("%d %d %d\n", header->end_step,header->begin_step, header->save_period);
    //header->totalframes = (header->end_step-header->begin_step)/header->save_period+1;
    fseek(fptr, sizeof(int32_t)*5, SEEK_CUR);
    fread(&(header->dt), sizeof(float), 1, fptr);
    fseek(fptr, sizeof(int32_t)*9, SEEK_CUR);
    fread(&buff, sizeof(int32_t), 1, fptr);
    header->charm_ver = (int)buff;
    fseek(fptr, sizeof(int32_t)*3, SEEK_CUR);
    fread(header->remark, 80, 1, fptr);
    fread(header->time_stamp, 80, 1, fptr);
    fseek(fptr, sizeof(int32_t)*2, SEEK_CUR);
    
    fread(&buff, sizeof(int32_t), 1, fptr);
    header->numbeads = (int)buff;
    fseek(fptr, sizeof(int32_t), SEEK_CUR);
       
    return header;
}

void* create_traj_dcd_handler(char* filename)
{
    traj_dcd_handler* ptr = (traj_dcd_handler*)malloc(sizeof(traj_dcd_handler));
    if(ptr == NULL)
    {
        fprintf(stderr, "error in malloc %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    memset(ptr, 0, sizeof(traj_dcd_handler));
    ptr->filename = (char*)malloc(strlen(filename)+1);
    strcpy(ptr->filename, filename);

    FILE* fptr = fopen(filename, "rb");
    if(fptr == NULL)
    {
        fprintf(stderr, "no file %s found\n", filename);
        exit(-1);        
    }
    ptr->fptr = fptr;
    ptr->header = read_dcd_header(fptr);
    //ptr->is_header_read = 1;

    ptr->capacity = 1024;

    ptr->unitcell = malloc(sizeof(double*)*ptr->capacity);
    memset(ptr->unitcell, 0, sizeof(double*)*ptr->capacity);

    ptr->coords   = malloc(sizeof(float*) *ptr->capacity);
    memset(ptr->coords, 0, sizeof(float*)*ptr->capacity);

    long int curr_pos = ftell(fptr);
    fseek(fptr, 0, SEEK_END);
    long int end = ftell(fptr);
    long int total_bytes = end-curr_pos;
    if(total_bytes % (ptr->header->totalframes))
    {
        fprintf(stderr,"total bytes %ld %d\n", total_bytes, ptr->header->totalframes);
        fprintf(stderr, "error in data %s %d\n", __FILE__, __LINE__);
        exit(-1);    
    }
    ptr->bytes_per_frame = total_bytes / (ptr->header->totalframes);
    //fprintf(stderr, "%d %ld\n", ptr->header->totalframes, ptr->bytes_per_frame);
    fseek(fptr, curr_pos, SEEK_SET);

    return ptr;
}

int get_num_frames(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    return handler->numframes;
}

float get_time_step(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    return handler->header->dt;
}

int get_save_period(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    return handler->header->save_period;
}

int get_num_atoms(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    return handler->header->numbeads;
}

float* get_coords(void* _handler, int f)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    if(f >= handler->numframes)
    {
        fprintf(stderr, "Index out of bound %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    else
        return handler->coords[f];
}

float** get_full_coords(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    return handler->coords;
}

double* get_unitcell(void* _handler, int f)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    if(f >= handler->numframes)
    {
        fprintf(stderr, "Index out of bound %s %d\n", __FILE__, __LINE__);
        exit(-1);
    }
    else
        return handler->unitcell[f];
}

//do not clean the buffer
void reset_traj_dcd(void* _handler)
{
   traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
   handler->numframes = 0;
}

//will clean the buffer
void clean_traj_dcd(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    int cap = handler->capacity;
    for(int f = 0; f < cap; ++f)
    {
        free(handler->unitcell[f]);
        free(handler->coords[f]);
    }    
    handler->numframes = 0;
}

void clean_traj_dcd_handler(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    free(handler->filename);
    fclose(handler->fptr);
    free(handler->header);
    clean_traj_dcd(_handler);
    free(handler->unitcell);
    free(handler->coords); 
    free(handler); 
}

void* create_traj_dcd_writer(char* filename)
{
    traj_dcd_writer* ptr = (traj_dcd_writer*)malloc(sizeof(traj_dcd_writer));
    memset(ptr, 0, sizeof(traj_dcd_writer));
    ptr->filename = malloc(sizeof(char)*(strlen(filename)+1));
    strcpy(ptr->filename, filename);
    ptr->fptr = fopen(ptr->filename, "wb");
    if(ptr->fptr == NULL)
    {
        fprintf(stderr, "can not open file %s\n", filename);
        exit(-1);
    }
    return ptr;
}

void clean_traj_dcd_writer(void* _writer)
{
    traj_dcd_writer* writer = (traj_dcd_writer*)_writer;
    free(writer->filename);
    fclose(writer->fptr);
    free(writer->coords);
    free(_writer);
}

void set_remark(void* _writer, char* remark)
{
    traj_dcd_writer* writer = (traj_dcd_writer*)_writer;
    strncpy(writer->remark, remark, 80);
}

void set_time_step(void* _writer, float dt)
{
    traj_dcd_writer* writer = (traj_dcd_writer*)_writer;
    writer->dt = dt;
    writer->is_dt_set = 1;
}

void set_save_period(void* _writer, int save_period)
{
    traj_dcd_writer* writer = (traj_dcd_writer*)_writer;
    writer->save_period = save_period;
    writer->is_save_period_set = 1;
}

void set_num_atoms(void* _writer, int num)
{
    traj_dcd_writer* writer = (traj_dcd_writer*)_writer;
    writer->numbeads = num;
    writer->coords = (float*)malloc(sizeof(float)*3*writer->numbeads);
    writer->xf = writer->coords;
    writer->yf = writer->coords+num;
    writer->zf = writer->coords+2*num;
    writer->is_numbeads_set = 1; 
}

static void fwrite_int32(FILE* fd, uint32_t i)
{
    fwrite(&i,sizeof(uint32_t),1,fd);
}

static void _write_dcd_header(traj_dcd_writer* writer)
{
    uint32_t out_integer;
    float out_float;
    char title_string[200];
    time_t cur_time;
    struct tm *tmbuf;
  
    int ntimestep = 0;
    FILE* fp = writer->fptr;

    out_integer = 84;
    fwrite_int32(fp,out_integer);
    strcpy(title_string,"CORD");
    fwrite(title_string,4,1,fp);
    fwrite_int32(fp,0);                    // NFILE = # of snapshots in file
    fwrite_int32(fp,ntimestep);            // START = timestep of first snapshot
    fwrite_int32(fp,writer->save_period);  // SKIP = interval between snapshots
    fwrite_int32(fp,ntimestep);            // NSTEP = timestep of last snapshot
    fwrite_int32(fp,0);                         // NAMD writes NSTEP or ISTART
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    out_float = writer->dt;
    fwrite(&out_float,sizeof(float),1,fp);
    fwrite_int32(fp,1);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,0);
    fwrite_int32(fp,24);                   // pretend to be Charmm version 24
    fwrite_int32(fp,84);
    fwrite_int32(fp,164);
    fwrite_int32(fp,2);
    strncpy(title_string,writer->remark,80);
    title_string[79] = '\0';
    fwrite(title_string,80,1,fp);
    cur_time=time(NULL);
    tmbuf=localtime(&cur_time);
    memset(title_string,' ',81);
    strftime(title_string,80,"REMARKS Created %d %B,%Y at %H:%M",tmbuf);
    fwrite(title_string,80,1,fp);
    fwrite_int32(fp,164);
    fwrite_int32(fp,4);
    fwrite_int32(fp,writer->numbeads);     // number of atoms in each snapshot
    fwrite_int32(fp,4);
    fflush(fp);
    writer->is_header_written = 1;
}

static void _write_header(traj_dcd_writer* writer, double* h)
{
    if(writer->is_header_written == 0)
    {
        if(!(writer->is_numbeads_set) || !(writer->is_save_period_set) || !(writer->is_dt_set))
        {
            fprintf(stderr, "some parameters are not set\n");
            exit(-1);
        } 
        _write_dcd_header(writer);
    }
    FILE* fp = writer->fptr;
    /*double dim[6];
    double alen = h[0];
    double blen = sqrt(h[5]*h[5] + h[1]*h[1]);
    double clen = sqrt(h[4]*h[4] + h[3]*h[3] + h[2]*h[2]);
    dim[0] = alen;
    dim[2] = blen;
    dim[5] = clen;
    dim[4] = (h[5]*h[4] + h[1]*h[3]) / blen/clen; // alpha
    dim[3] = (h[0]*h[4]) / alen/clen;             // beta
    dim[1] = (h[0]*h[5]) / alen/blen;             // gamma
    */
    uint32_t out_integer = 48;
    fwrite_int32(fp,out_integer);
    fwrite(h,out_integer,1,fp);
    fwrite_int32(fp,out_integer);
}

static void _write_frame(traj_dcd_writer* writer, float* buff)
{
    int m = 0;
    int n = writer->numbeads;
    float* xf, *yf, *zf;
    xf = writer->xf;
    yf = writer->yf;
    zf = writer->zf;
    FILE* fp = writer->fptr;
    for (int i = 0; i < n; i++) 
    {
        xf[i] = buff[m++];
        yf[i] = buff[m++];
        zf[i] = buff[m++];
    }
    // write coords

    uint32_t out_integer = n*sizeof(float);
    fwrite_int32(fp,out_integer);
    fwrite(xf,out_integer,1,fp);
    fwrite_int32(fp,out_integer);
    fwrite_int32(fp,out_integer);
    fwrite(yf,out_integer,1,fp);
    fwrite_int32(fp,out_integer);
    fwrite_int32(fp,out_integer);
    fwrite(zf,out_integer,1,fp);
    fwrite_int32(fp,out_integer);

    // update NFILE and NSTEP fields in DCD header
    writer->numframes++;
    out_integer = writer->numframes;
    fseek(fp,NFILE_POS,SEEK_SET);
    fwrite_int32(fp,out_integer);
    out_integer = writer->save_period*(writer->numframes-1);
    fseek(fp,NSTEP_POS,SEEK_SET);
    fwrite_int32(fp,out_integer);
    fseek(fp,0,SEEK_END);
}

void write_data(void* _writer, float* _coords, double* unitcell)
{
    traj_dcd_writer* writer = (traj_dcd_writer*)_writer; 
    _write_header(writer, unitcell);
    _write_frame(writer, _coords);
    fflush(writer->fptr);
}



#ifdef debug
void show_dcd_header(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    dcd_header* header = handler->header;
    printf("title %s\n", header->title);
    printf("remakr %s\n", header->remark);
    printf("time stamp %s\n", header->time_stamp);
    printf("begin step %d end step %d\n", header->begin_step, header->end_step);
    printf("save period %d\n", header->save_period);
    printf("total frames %d\n", header->totalframes);
    printf("charm version %d\n", header->charm_ver);
    printf("time step %f\n", header->dt);
    printf("total beads %d\n", header->numbeads);
}

void show_dcd_trajs(void* _handler)
{
    traj_dcd_handler* handler = (traj_dcd_handler*)_handler;
    printf("file name %s\n", handler->filename);
    printf("number of frames %d\n", handler->numframes);
    printf("bytes per frame %ld\n", handler->bytes_per_frame);
    int numframes = handler->numframes;
    for(int f = 0; f < numframes; ++f)
    {
        double* unitcell = handler->unitcell[f];
        printf("unit cell %f %f %f %f %f %f\n", unitcell[0], unitcell[1], unitcell[2], unitcell[3], unitcell[4], unitcell[5]);
        float* coords = handler->coords[f];
        int numbeads = handler->header->numbeads;
        for(int i = 0; i < numbeads; ++i)
            printf("coords %f %f %f\n", coords[3*i+0], coords[3*i+1], coords[3*i+2]);
    }
}

#endif
