#include <stdlib.h>
#include <stdio.h>
#include "traj_dcd_handler.h"
int main(int argc, char** argv)
{
    if(argc < 2)
    {
        fprintf(stderr, "erro number of arguments\n");
        exit(-1);
    }
    void* handler = create_traj_dcd_handler(argv[1]);
    for(int i = 0; i < 50; i += 10)
    {
        read_dcd(handler, i, i+10, 1);
        #ifdef debug
        show_dcd_header(handler);
        show_dcd_trajs(handler);
        #endif
        reset_traj_dcd(handler);
    }
    read_dcd(handler, 10, 40000, 1);
    #ifdef debug
    show_dcd_header(handler);
    show_dcd_trajs(handler);
    #endif

    clean_traj_dcd_handler(handler);
    return 0;
}
