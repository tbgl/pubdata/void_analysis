#ifndef TRAJ_DCD_HANDLER_H_
#define TRAJ_DCD_HANDLER_H_

void* create_traj_dcd_handler(char*);
int read_dcd(void*, int, int, int);
void reset_traj_dcd(void*);
void clean_traj_dcd(void*);
void clean_traj_dcd_handler(void*);
int get_num_frames(void*);
int get_num_atoms(void*);
float* get_coords(void*, int);
float** get_full_coords (void*);
double* get_unitcell(void*, int);
float get_time_step(void*);
int get_save_period(void*);
void* create_traj_dcd_writer(char*);
void write_data(void*, float*, double*);
void clean_traj_dcd_writer(void*);
void set_remark(void*, char*);
void set_time_step(void*, float);
void set_save_period(void*, int);
void set_num_atoms(void*, int);
void write_data(void*, float*, double*);
#ifdef debug
void show_dcd_header(void*);
void show_dcd_trajs(void*); 
#endif
#endif
